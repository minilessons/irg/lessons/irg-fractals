/**
 * Main constants used for this project.
 * This constructor holds default values for all constants.
 *
 * Several constants object are provided: "colors", "fonts", "strings", "numeric" and "keyCodes".
 * They offer various elements including a lot of colors, fonts, string constants, numeric constants and common key codes for keyboard presses.
 */
class Constants {
    constructor(colors = {
                    black: "#000000",
                    yellow: "#FFFF00",
                    red: "#FF0000",
                    white: "#FFFFFF",
                    orange: "#fa9301",
                    awesome: "#8836ba",
                    gray: "#AF1E37",
                    darkRed: "#d02b2a",
                    purple: "#8e44ad",
                    emerald: "#2ecc71",
                    midnight: "#2c3e50",
                    dodgerBlue: "#1E90FF",
                    flipD: "#1E90FF",
                    flipT: "#886F98",
                    diagramArrow: "#da0000",
                    flipSR: "#CB3A34",
                    flipJK: "#2ecc71",
                    registerColor: "#50928c",
                    clickBoxColor: "#746dc5",
                    darkBlue: "#00008B",
                    lightSkyBlue: "#87CEFA",
                    glowColor: "rgba(255, 0, 0, 0.5)",
                    fullTransparent: "rgba(255, 255, 255, 0)",
                    defaultPixel: "#b5e2cb",
                    selectedPixel: "#b65bab",
                    correctColor: "#71bf9a",
                    successBox: "#8cdf7b",
                    infoNiceBlue: "#2196F3",
                    pixelActiveInput: "#df120e",
                    startPoints: "#e23e41",
                    selectorColor: "#12ACBD",
                    gridColor: "#dda8d7",
                },
                fonts = {
                    pixelFont: "14px bold Schoolbook",
                    normalFont: "14px Schoolbook",
                    arialFont: "12px Arial",
                    startEndPoints: "24px Schoolbook",
                    axesFont: "12px Schoolbook"
                },
                strings = {
                    EMPTY: "",
                },
                numeric = {},
                keyCodes = {
                    left: 37,
                    up: 38,
                    right: 39,
                    down: 40,
                    enter: 13,
                    E: 69
                },
                smallLetters = {
                    /* Various text subscripts and superscripts */
                    /* Subscript <sub> x </sub> numbers */
                    sup0: '\u2070',
                    sup1: '\u00B9',
                    sup2: '\u00B2',
                    sup3: '\u00B3',
                    sup4: '\u2074',
                    sup5: '\u2075',
                    sup6: '\u2076',
                    sup7: '\u2077',
                    sup8: '\u2078',
                    sup9: '\u2079',

                    /* Superscript <sup> x </sup> numbers */
                    sub0: '\u2080',
                    sub1: '\u2081',
                    sub2: '\u2082',
                    sub3: '\u2083',
                    sub4: '\u2084',
                    sub5: '\u2085',
                    sub6: '\u2086',
                    sub7: '\u2087',
                    sub8: '\u2088',
                    sub9: '\u2089',

                    // Small sub letters
                    subA: '\u2090',
                    subE: '\u2091',
                    subO: '\u2092',
                    subM: '\u2098',
                    subN: '\u2099',

                    supN: '\u207F',

                    // Other signs
                    subPlus: '\u208A',
                    subMinus: '\u208B',
                    subBracketLeft: '\u208F',
                    supBracketLeft: '\u207D',
                    subBracketRight: '\u208E',
                    supBracketRight: '\u207E',
                },
                widths = {}) {
        this.colors = colors;
        this.fonts = fonts;
        this.strings = strings;
        this.numeric = numeric;
        this.keyCodes = keyCodes;
        this.smallLetters = smallLetters;
        this.widths = widths;

        this.getRGBAColor = function (name) {
            try {
                return convertHexToRGBA(this.colors[name]);
            } catch (err) {
                throw ("Undefined color for name: " + name);
            }
        };

        this.getColor = function (name) {
            try {
                return this.colors[name];
            } catch (err) {
                throw ("Undefined color for name: " + name);
            }
        };

        this.getString = function (name) {
            try {
                return this.strings[name];
            } catch (err) {
                throw ("Undefined color for name: " + name);
            }
        };

        this.getKeyCode = function (name) {
            try {
                return this.keyCodes[name];
            } catch (err) {
                throw ("Undefined color for name: " + name);
            }
        };

        this.getFont = function (name) {
            try {
                return this.fonts[name];
            } catch (err) {
                throw ("Undefined color for name: " + name);
            }
        };

        this.getNumeric = function (name) {
            try {
                return this.numeric[name];
            } catch (err) {
                throw ("Undefined color for name: " + name);
            }
        };
    }
}


/**
 * Gets current project constants using additional specified constants in specs as well as other constants.
 *
 * @returns {Constants} Constants with specified constants
 */
function getConstants() {
    let colors = {
        black: "#000000",
        yellow: "#FFFF00",
        red: "#FF0000",
        white: "#FFFFFF",
        orange: "#fa9301",
        awesome: "#8836ba",
        gray: "#AF1E37",
        darkRed: "#d02b2a",
        purple: "#8e44ad",
        emerald: "#2ecc71",
        midnight: "#2c3e50",
        dodgerBlue: "#1E90FF",
        flipD: "#1E90FF",
        flipT: "#886F98",
        diagramArrow: "#da0000",
        flipSR: "#CB3A34",
        flipJK: "#2ecc71",
        registerColor: "#50928c",
        clickBoxColor: "#746dc5",
        darkBlue: "#00008B",
        lightSkyBlue: "#87CEFA",
        glowColor: "rgba(255, 0, 0, 0.5)",
        fullTransparent: "rgba(255, 255, 255, 0)",
        defaultPixel: "#b5e2cb",
        selectedPixel: "#b65bab",
        correctColor: "#71bf9a",
        successBox: "#8cdf7b",
        infoNiceBlue: "#2196F3",
        pixelActiveInput: "#df120e",
        startPoints: "#e23e41",
        selectorColor: "#12ACBD",
        gridColor: "#dda8d7",
        transparency: {
            transparent: 0.7,
            nonTransparent: 1
        },
    };
    let fonts = {
        normalFont: "14px Schoolbook",
        arialFont: "12px Arial",
        axesFont: "12px Schoolbook",
        schoolBook: "14px Schoolbook"
    };

    let strings = {
        EMPTY: "",
        collapsedSymbol: "&#9658;",
        expandedSymbol: "&#9660;",
        uidPostfix: "!.!",
        noRetValue: "No return value.",
        degreeSymbol: "&deg;",
    };

    let numeric = {
        defaultFOV: (60),
        mathPrecision: 6,
    };
    let keyCodes = {
        escape: 27,
        left: 37,
        up: 38,
        right: 39,
        down: 40,
        enter: 13,
        E: 69,
        A: 65,
        S: 83,
        D: 68,
        W: 87,
        I: 73,
        D: 68,
        R: 82,
        H: 72,
        G: 71,
        
        // Smybols 
        plus: 61, // General plus (187?)
        minus: 173, // General minus
        numpadPlus: 107, // Numpad Plus
        numpadMinus: 109, // Numpad Minus


        // Function keys
        F1: 112,
        F2: 113,
    };
    let smallLetters = {
        /* Various text subscripts and superscripts */
        /* Subscript <sub> x </sub> numbers */
        sup0: '\u2070',
        sup1: '\u00B9',
        sup2: '\u00B2',
        sup3: '\u00B3',
        sup4: '\u2074',
        sup5: '\u2075',
        sup6: '\u2076',
        sup7: '\u2077',
        sup8: '\u2078',
        sup9: '\u2079',

        /* Superscript <sup> x </sup> numbers */
        sub0: '\u2080',
        sub1: '\u2081',
        sub2: '\u2082',
        sub3: '\u2083',
        sub4: '\u2084',
        sub5: '\u2085',
        sub6: '\u2086',
        sub7: '\u2087',
        sub8: '\u2088',
        sub9: '\u2089',

        // Small sub letters
        subA: '\u2090',
        subE: '\u2091',
        subO: '\u2092',
        subM: '\u2098',
        subN: '\u2099',

        supN: '\u207F',

        // Other signs
        subPlus: '\u208A',
        subMinus: '\u208B',
        subBracketLeft: '\u208F',
        supBracketLeft: '\u207D',
        subBracketRight: '\u208E',
        supBracketRight: '\u207E',
    };

    let widths = {
        axisStep: 0.2,
        axisWidth: 2
    };

    return new Constants(colors, fonts, strings, numeric, keyCodes, smallLetters, widths);
}

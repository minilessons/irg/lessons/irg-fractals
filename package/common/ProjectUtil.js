const COLOR_SHEMES = {
    SIMPLE_RED: 0,
    WONDERS_1: 1,
    WONDERS_2: 2,
    BLACK_WHITE: 3,
    HISTOGRAM: 4,
    PALLETE: 5,
    GRAY: 6
};

const L_SYSTEM_TYPE = {
    TRIANGLE_SIERPINSKI: 0,
    PLANT_1: 1,
    TREE_1: 2,
    KOCH_CURVE: 3,
    KOCH_CURVE_SNOWFLAKE: 4
};


const IFS_FRACTAL_TYPE = {
    FERN: 0,
    TRIANGLE_SIERPINSKI: 1,
    SIERPINSKI_CARPET: 2
};


const MOUSE_CLICK = {
    LEFT: 0,
    MIDDLE: 1,
    RIGHT: 2
};


/**
 * Zooms the mandelbrot fractal around the specified complex point which will be at the center of the newly generated fractal.
 * @param complexPoint the point in complex plane where the user clicked
 * @param zoomScale the scale of the zoom
 * @param myState the state in which this zoom is executed
 */
function zoomWith(complexPoint, zoomScale, myState) {
    let frConf = myState.fractalConfig;
    let scale = zoomScale || frConf.defaultZoomScale;
    // Looks better with same scale ww == wh!
    let uDiff = frConf.uMax - frConf.uMin;
    let vDiff = frConf.vMax - frConf.vMin;
    let ww = uDiff / scale;
    let wh = vDiff / scale;
    let cx = complexPoint.x;
    let cy = complexPoint.y;
    frConf.uMin = cx - ww / 2.0;
    frConf.uMax = cx + ww / 2.0;
    frConf.vMin = cy - wh / 2.0;
    frConf.vMax = cy + wh / 2.0;
    /* console.log("New draw area:",
         frConf.uMin,
         frConf.uMax,
         frConf.vMin,
         frConf.vMax)*/
}

/**
 * Helper function for calculating grid origin.
 * @param can canvas to which origin is relative to
 * @param step gap between grid lines
 * @returns {{x: number, y: number}} point x, y located at 0, 0 in local grid system.
 */
function calcOrigin(can, step) {
    let xCoord = Math.floor(step * Math.floor((Math.floor((can.width / step))) / 2));
    let yCoord = Math.floor(step * Math.floor((Math.floor((can.height / step))) / 2));
    return { x: xCoord, y: yCoord }
}


/**
 * Function for constructing coordinate array to use for webGL buffer data.
 *
 * @param s step used for line distance
 * @param tickSize the length of a tick in pixels
 * @returns {{values: Array, colors: Array}}
 */

function getCoordinateArray(canvas, s, tickSize = 15) {
    let arr = [];

    let step = s;
    let verStart = 0;
    let horStart = 0;
    let verEnd = canvas.height;
    let horEnd = canvas.width;
    let halfTickSize = Math.floor(tickSize / 2);

    let o = calcOrigin(canvas, step);
    let xAxis = o.x;

    let axisX = [];
    for (let c = 0; c < (horEnd + step); c += step) {
        if (c === xAxis) {
            axisX = [c, verStart, c, verEnd];

        } else {
            arr.push(c);
            arr.push(o.y - halfTickSize);
            arr.push(c);
            arr.push(o.y + halfTickSize);
        }
    }

    let yAxis = o.y;
    let axisY = [];
    for (let y = 0; y < (step + verEnd); y += step) {
        if (y === yAxis) {
            axisY = [horStart, y, horEnd, y];
        } else {
            arr.push(o.x - halfTickSize);
            arr.push(y);
            arr.push(o.x + halfTickSize);
            arr.push(y);
        }
    }

    arr.push(axisX[0], axisX[1], axisX[2], axisX[3]);
    arr.push(axisY[0], axisY[1], axisY[2], axisY[3]);
    return arr;
}


function initCoordinatesWithSuffix(state, suffix) {
    let s = suffix || "";
    state.complexX = document.getElementById("complex-x" + s);
    state.complexY = document.getElementById("complex-y" + s);
    state.rasterX = document.getElementById("raster-x" + s);
    state.rasterY = document.getElementById("raster-y" + s);
}


/**
 * Converts a raster coordinate to the complex plane coordinates.
 * @param config the configuration holding max and min values for both coordinate planes
 * @param x the x raster coordintae
 * @param y the y raster coordinate
 * @returns {{x: *, y: *}} the complex point converted from raster coordinates
 */
function getComplexPointFromRaster(config, x, y) {
    let xW = config.xMax - config.xMin;
    let yH = config.yMax - config.yMin;
    let uW = config.uMax - config.uMin;
    let vH = config.vMax - config.vMin;
    let cx = (x - config.xMin) / (xW) * (uW) + config.uMin;
    let cy = (y - config.yMin) / (yH) * (vH) + config.vMin;

    return {
        x: cx,
        y: cy
    }
}

function affineTransformABtoCD(x, a, b, c, d) {
    // return x;
    return ((x - a) * ((d - c) / (b - a))) + c;
}


function disableContextMenu(canvas) {
    canvas.oncontextmenu = function (e) {
        e.preventDefault();
    };
}


function isMouseClick(e, clickIndex) {
    e = e || window.event;
    if ("which" in e) {
        return e.which === (clickIndex + 1);
    } else if ("button" in e) {
        return e.button === clickIndex;
    } else {
        throw new Error("Cannot get mouse click.");
    }
}


function isLeftClick(e) {
    return isMouseClick(e, MOUSE_CLICK.LEFT);
}


function isRightClick(e) {
    return isMouseClick(e, MOUSE_CLICK.RIGHT);
}

function initZoomLockSetting(renderer, id) {
    let zoomLockCheckbox = document.getElementById(id);
    zoomLockCheckbox.onclick = function (e) {
        renderer.fractalConfig.zoomLocked = e.target.checked;
    }

    zoomLockCheckbox.checked = renderer.fractalConfig.zoomLocked;
}

function initSelectValuePicker(renderer, items, id, currentItemID) {
    let selectOptions = document.getElementById(id);
    // `<option value="colorShemeId">colorShemeName</option>`
    for (let item in items) {
        let optionTag = document.createElement("option");
        optionTag.setAttribute("value", item.toString());
        optionTag.innerHTML = item.toLowerCase();
        selectOptions.appendChild(optionTag);
    }
    selectOptions.selectedIndex = renderer.fractalConfig[currentItemID];
    selectOptions.onchange = function (e) {
        renderer.fractalConfig[currentItemID] = e.target.selectedIndex;
        renderer.config.updateAll = true;
        renderer.updateLater();
    };
}


function getRasterVertices(config) {
    let vertices = [];
    for (let y = config.yMin; y < config.yMax; y++) {
        for (let x = config.xMin; x < config.xMax; x++) {
            vertices.push(x, y);
        }
    }
    return vertices;
}


function triangulateLine(x0, y0, x1, y1, thickness = 1) {
    let halfThickness = thickness / 2;
    let dx = x1 - x0;
    let dy = y1 - y0;
    let lineLength = Math.sqrt(dx * dx + dy * dy);
    dx /= lineLength;
    dy /= lineLength;

    // Perpendicular vector is (-dy, dx)
    const px = halfThickness * (-dy); //perpendicular vector with lenght thickness * 0.5
    const py = halfThickness * dx;

    let topLeft = new Vector2D(x0 - px, y0 + py);
    let bottomLeft = new Vector2D(x0 + px, y0 - py);
    let topRight = new Vector2D(x1 - px, y1 + py);
    let bottomRight = new Vector2D(x1 + px, y1 - py);

    let r = [];
    r = r.concat(topLeft.toArray());
    r = r.concat(bottomLeft.toArray());
    r = r.concat(topRight.toArray());
    r = r.concat(bottomLeft.toArray());
    r = r.concat(topRight.toArray());
    r = r.concat(bottomRight.toArray());
    return r;
}


function triangulateRectangle(x, y, width, height) {
    return [
        x, y,
        x + width, y,
        x + width, y + height,

        x, y,
        x, y + height,
        x + width, y + height
    ]
}


/**
 * Replaces define directives in vertex shader.
 * In this case only depth limit is set up.
 */
function addFills(gl, source, type, existing, replaceStr) {
    if (type === gl.VERTEX_SHADER) {
        return source.replace(existing, replaceStr);
    } else {
        return source;
    }
}


function updateVertexBuffer(gl, obj, config) {
    obj.bufferData = getRasterVertices(config);
    updateWebGLBuffer(gl, obj);
}


function renderClickCallback(e, state) {
    let btn = e.target;
    let btnOldDesc = btn.innerHTML;
    btn.disabled = true;
    let oldColor = btn.style.backgroundColor;
    btn.style.backgroundColor = "#F0F000";
    btn.innerHTML = "Rendering...";

    if ((state.config !== undefined) && (state.config.updateAll !== undefined)) {
        state.config.updateAll = true;
    }
    state.updateLater(function () {
        btn.innerHTML = btnOldDesc;
        btn.disabled = false;
        btn.style.backgroundColor = oldColor;
    });
}

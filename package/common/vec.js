function $vec(data) {
    if (!(this instanceof $vec)) {
        return new $vec(data);
    }

    if (data instanceof $vec) {
        this.elements = [].concat(data.elements);
    } else if (Array.isArray(data)) {
        this.elements = data;
    } else {
        throw "Expected array or $vec object."
    }
}


$vec.elems = function (a) {
    if (Array.isArray(a)) {
        return a;
    }

    if (a instanceof $vec) {
        return a.elements;
    }

    throw "Expected array or vec$ object.";
};

$vec.prototype.elems = function () {
    return this.elements;
};

$vec.checkDimensions = function (a, b) {
    if (a.length !== b.length) {
        throw "Incompatible vector/matrix dimensions.";
    }
};

$vec.add = function (a, b) {
    $vec.checkDimensions(a, b);

    let result = [];
    let size = a.length;

    for (let c = 0; c < size; c++) {
        result[c] = (a[c] + b[c]);
    }

    return result;
};

$vec.prototype.add = function (other) {
    return $vec($vec.add(this.elements, $vec.elems(other)));
};


$vec.sub = function (a, b) {
    $vec.checkDimensions(a, b);

    let result = [];
    let size = a.length;

    for (let c = 0; c < size; c++) {
        result[c] = (a[c] - b[c]);
    }

    return result;
};

$vec.prototype.sub = function (other) {
    return $vec($vec.sub(this.elements), $vec.elems(other));
};

$vec.dot = function (a, b) {
    $vec.checkDimensions(a, b);

    let result = 0;
    let size = a.length;

    for (let c = 0; c < size; c++) {
        result += (a[c] * b[c]);
    }

    return result;
};

$vec.prototype.dot = function (other) {
    return $vec.dot(this.elements, $vec.elems(other));
};

$vec.multiplyWithScalar = function (a, scalar) {
    let result = [];
    let size = a.length;

    for (let c = 0; c < size; c++) {
        result.push(scalar * a[c]);
    }

    return result;
};

$vec.prototype.multiplyWithScalar = function (scalar) {
    return $vec($vec.multiplyWithScalar(this.elements, scalar));
};

$vec.norm = function (a) {
    let result = 0;
    let size = a.length;

    for (let c = 0; c < size; c++) {
        result += (a[c] * a[c]);
    }

    return Math.sqrt(result);
};

$vec.prototype.norm = function () {
    return $vec.norm(this.elements);
};

$vec.normalize = function (a) {
    let result = [];
    let size = a.length;
    let norm = $vec.norm(a);

    for (let c = 0; c < size; c++) {
        result.push(a[c] / norm);
    }

    return result;
};

$vec.prototype.normalize = function () {
    return $vec($vec.normalize(this.elements));
};

$vec.are3D = function (a, b) {
    return !(a.length !== 3 || b.length !== 3);
};

$vec.cross = function (a, b) {
    if (!$vec.are3D(a, b)) {
        throw "Incompatible vectors dimensions for cross product.";
    }

    return [
        a[1] * b[2] - a[2] * b[1],
        a[2] * b[0] - a[0] * b[2],
        a[0] * b[1] - a[1] * b[0]
    ];
};

$vec.prototype.cross = function (other) {
    return $vec($vec.cross(this.elements, $vec.elems(other)));
};

$vec.prototype.toString = function () {
    return " [" + this.elements.join(",") + "] ";
};

$vec.to3D = function (a) {
    let elem = $vec.elems(a);

    if (elem.length === 3) {
        // Three dimensional vector, return it.
        return $vec([].concat(elem));
    }

    if (elem.length === 4) {
        // Four dimensional vector, divide by homogeneous coordinate
        let h = elem[3];
        return $vec(elem[0] / h, elem[1] / h, elem[2] / h);
    }

    throw "Expected three or four dimensional vector.";
};

$vec.prototype.to3D = function () {
    return $vec.to3D(this);
};

$vec.round = function (a) {
    let size = a.length;
    let result = [];
    for (let c = 0; c < size; c++) {
        result.push(Math.round(a[c]));
    }

    return result;
};

$vec.prototype.round = function () {
    return $vec($vec.round(this.elements));
};

$vec.componentMultiply = function (a, b) {
    let result = [];
    let size = a.length;

    for (let c = 0; c < size; c++) {
        result.push(a[c] * b[c]);
    }

    return result;
};

$vec.prototype.componentMultiply = function (other) {
    return $vec($vec.componentMultiply(this.elements, $vec.elems(other)));
};

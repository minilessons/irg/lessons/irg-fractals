class Vector2D {

    constructor(x, y) {
        this.x = x;
        this.y = y;
    }


    translate(offset) {
        this.x += offset.x;
        this.y += offset.y;
    }


    translated(offset) {
        let aVector = this.copy();
        aVector.translate(offset);
        return aVector;
    }


    rotate(angle) {
        let radiansAngle = degToRad(angle);

        let newX = this.x * Math.cos(radiansAngle) - this.y * Math.sin(radiansAngle);
        let newY = this.x * Math.sin(radiansAngle) + this.y * Math.cos(radiansAngle);

        this.x = newX;
        this.y = newY;
    }


    rotated(angle) {
        let aVector = this.copy();
        aVector.rotate(angle);
        return aVector;
    }


    scale(scaler) {
        this.x *= scaler;
        this.y *= scaler;
    }


    scaled(scaler) {
        let aVector = this.copy();
        aVector.scale(scaler);
        return aVector;
    }


    copy() {
        return new Vector2D(this.x, this.y);
    }


    toArray() {
        return [this.x, this.y]
    }
}

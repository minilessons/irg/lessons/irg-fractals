function $m4(data) {
    if (!(this instanceof $m4)) {
        return new $m4(data);
    }

    if (data instanceof $m4) {
        this.elements = [
            [].concat(data.elements[0]),
            [].concat(data.elements[1]),
            [].concat(data.elements[2]),
            [].concat(data.elements[3])
        ];
    } else if (Array.isArray(data)) {
        $m4.check(data, 4, 4);
        this.elements = data;
    } else {
        throw "Expected 4x4 array or $m4 object."
    }
}


$m4.copy = function (data) {
    let arr = null;
    if (data instanceof $m4) {
        arr = data.elements;
    } else if (Array.isArray(data)) {
        arr = data;
    } else {
        throw 'Expected 4x4 array or $m4 object.';
    }
    let res = [];
    let n = arr.length;
    for (let i = 0; i < n; i++) {
        res.push([].concat(arr[i]));
    }
    return res;
};

$m4.check = function (m, r, c) {
    if (m.length !== r) throw "Expected " + r + "-row matrix; got " + m.length + ".";
    for (let i = 0; i < r; i++) {
        if (m[i].length !== c) throw "Expected " + r + "x" + c + " matrix; found col with " + m[i].length + " elements.";
    }
};

$m4.elems = function (data) {
    if (Array.isArray(data)) {
        $m4.check(data, 4, 4);
        return data;
    }
    if (data instanceof $m4) {
        return data.elements;
    }
    throw "Expected 4x4 array or $m4 object.";
};


$m4.prototype.elems = function () {
    return this.elements;
};

$m4.add = function (m1, m2) {
    m1 = $m4.elems(m1);
    m2 = $m4.elems(m2);
    $m4.check(m1, 4, 4);
    $m4.check(m2, 4, 4);
    return [
        [m1[0][0] + m2[0][0], m1[0][1] + m2[0][1], m1[0][2] + m2[0][2], m1[0][3] + m2[0][3]],
        [m1[1][0] + m2[1][0], m1[1][1] + m2[1][1], m1[1][2] + m2[1][2], m1[1][3] + m2[1][3]],
        [m1[2][0] + m2[2][0], m1[2][1] + m2[2][1], m1[2][2] + m2[2][2], m1[2][3] + m2[2][3]],
        [m1[3][0] + m2[3][0], m1[3][1] + m2[3][1], m1[3][2] + m2[3][2], m1[3][3] + m2[3][3]]
    ];
};

$m4.prototype.add = function (m) {
    return $m4($m4.add(this.elements, $m4.elems(m)));
};

$m4.sub = function (m1, m2) {
    m1 = $m4.elems(m1);
    m2 = $m4.elems(m2);
    $m4.check(m1, 4, 4);
    $m4.check(m2, 4, 4);
    return [
        [m1[0][0] - m2[0][0], m1[0][1] - m2[0][1], m1[0][2] - m2[0][2], m1[0][3] - m2[0][3]],
        [m1[1][0] - m2[1][0], m1[1][1] - m2[1][1], m1[1][2] - m2[1][2], m1[1][3] - m2[1][3]],
        [m1[2][0] - m2[2][0], m1[2][1] - m2[2][1], m1[2][2] - m2[2][2], m1[2][3] - m2[2][3]],
        [m1[3][0] - m2[3][0], m1[3][1] - m2[3][1], m1[3][2] - m2[3][2], m1[3][3] - m2[3][3]]
    ];
};

$m4.prototype.sub = function (m) {
    return $m4($m4.sub(this.elements, $m4.elems(m)));
};

$m4.transpose = function (m) {
    m = $m4.elems(m);
    $m4.check(m, 4, 4);
    return [
        [m[0][0], m[1][0], m[2][0], m[3][0]],
        [m[0][1], m[1][1], m[2][1], m[3][1]],
        [m[0][2], m[1][2], m[2][2], m[3][2]],
        [m[0][3], m[1][3], m[2][3], m[3][3]]
    ];
};

$m4.prototype.transpose = function () {
    return $m4($m4.transpose(this.elements));
};

$m4.multiplyWithScalar = function (m, s) {
    m = $m4.elems(m);
    $m4.check(m, 4, 4);
    return [
        [m[0] * s, m[1] * s, m[2] * s, m[3] * s],
        [m[4] * s, m[5] * s, m[6] * s, m[7] * s],
        [m[8] * s, m[9] * s, m[10] * s, m[11] * s],
        [m[12] * s, m[13] * s, m[14] * s, m[15] * s]
    ];
};

$m4.prototype.multiplyWithScalar = function (s) {
    return $m4.multiplyWithScalar(this.elements, s);
};

$m4.mul = function (m1, m2) {
    m1 = $m4.elems(m1);
    m2 = $m4.elems(m2);
    $m4.check(m1, 4, 4);
    $m4.check(m2, 4, 4);
    let sum = 0;
    let res = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];
    for (let r = 0; r < 4; r++) {
        for (let c = 0; c < 4; c++) {
            sum = 0;
            for (let k = 0; k < 4; k++) {
                sum += m1[r][k] * m2[k][c];
            }
            res[r][c] = sum;
        }
    }
    return res;
};

$m4.prototype.mul = function (m) {
    return $m4($m4.mul(this.elements, $m4.elems(m)));
};

/**
 * Multiplies matrix * vector. Vector can be four or three dimensional. Result is in four dimensions.
 */
$m4.preMul = function (m, v) {
    m = $m4.elems(m);
    v = $vec.elems(v);
    $m4.check(m, 4, 4);
    if (v.length === 4) {
        return [
            m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2] + m[0][3] * v[3],
            m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2] + m[1][3] * v[3],
            m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2] + m[2][3] * v[3],
            m[3][0] * v[0] + m[3][1] * v[1] + m[3][2] * v[2] + m[3][3] * v[3]
        ];
    } else if (v.length === 3) {
        return [
            m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2] + m[0][3],
            m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2] + m[1][3],
            m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2] + m[2][3],
            m[3][0] * v[0] + m[3][1] * v[1] + m[3][2] * v[2] + m[3][3]
        ];
    } else {
        throw "Expected 3 or 4-dim vector.";
    }
};

$m4.prototype.preMul = function (v) {
    return $vec($m4.preMul(this.elements, $vec.elems(v)));
};

/**
 * Multiplies vector * matrix. Vector can be four or three dimensional. Result is in four dimensions.
 */
$m4.postMul = function (m, v) {
    m = $m4.elems(m);
    v = $vec.elems(v);
    $m4.check(m, 4, 4);
    if (v.length === 4) {
        return [
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2] + m[3][0] * v[3],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2] + m[3][1] * v[3],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2] + m[3][2] * v[3],
            m[0][3] * v[0] + m[1][3] * v[1] + m[2][3] * v[2] + m[3][3] * v[3]
        ];
    } else if (v.length === 3) {
        return [
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2] + m[3][0],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2] + m[3][1],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2] + m[3][2],
            m[0][3] * v[0] + m[1][3] * v[1] + m[2][3] * v[2] + m[3][3]
        ];
    } else {
        throw "Expected 3 or 4-dim vector.";
    }
};

$m4.prototype.postMul = function (v) {
    return $vec($m4.postMul(this.elements, $vec.elems(v)));
};

/**
 * Saves elements from first row, then second...
 */
$m4.asArrayByRows = function (m) {
    m = $m4.elems(m);
    $m4.check(m, 4, 4);
    return [m[0][0], m[0][1], m[0][2], m[0][3], m[1][0], m[1][1], m[1][2], m[1][3], m[2][0], m[2][1], m[2][2], m[2][3], m[3][0], m[3][1], m[3][2], m[3][3]];
};

$m4.prototype.asArrayByRows = function () {
    return $m4.asArrayByRows(this.elements);
};

/**
 * Saves elements from first column then second... uniformMatrix4fv are expecting this way of matrix array.
 */
$m4.asArrayByColumns = function (m) {
    m = $m4.elems(m);
    $m4.check(m, 4, 4);
    return [m[0][0], m[1][0], m[2][0], m[3][0], m[0][1], m[1][1], m[2][1], m[3][1], m[0][2], m[1][2], m[2][2], m[3][2], m[0][3], m[1][3], m[2][3], m[3][3]];
};

$m4.prototype.asArrayByColumns = function () {
    return $m4.asArrayByColumns(this.elements);
};

$m4.prototype.toString = function () {
    return '[' + $vec(this.elements[0]) + ', ' + $vec(this.elements[1]) + ', ' + $vec(this.elements[2]) + ', ' + $vec(this.elements[3]) + ']';
};

$m4.prototype.float32array = function () {
    return new Float32Array(this.asArrayByColumns());
};

$m4.float32array = function (m) {
    return new Float32Array($m4.asArrayByColumns(m));
};

$m4.inv = function (m) {
    function mkeye(n) {
        let res = [];
        for (let i = 0; i < n; i++) {
            let row = [];
            for (let j = 0; j < n; j++) row.push(i === j ? 1 : 0);
            res.push(row);
        }
        return res;
    }


    function findMaxi(m1, c) {
        let max = Math.abs(m1[c][c]), maxi = c;
        let n = m1.length;
        for (let k = c + 1; k < n; k++) {
            let maxcand = Math.abs(m1[k][c]);
            if (maxcand > max) {
                maxi = k;
                maxcand = max;
            }
        }
        return maxi;
    }


    function addRow(m1, r1, r2) {
        let n = m1.length;
        for (let k = 0; k < n; k++) {
            m1[r1][k] += m1[r2][k];
        }
    }


    function addScaledRow(m1, r1, r2, s) {
        let n = m1.length;
        for (let k = 0; k < n; k++) {
            m1[r1][k] += m1[r2][k] * s;
        }
    }


    function scaleRow(m1, r1, s) {
        let n = m1.length;
        for (let k = 0; k < n; k++) {
            m1[r1][k] *= s;
        }
    }


    m = $m4.copy($m4.elems(m));
    let d = m.length;
    let res = mkeye(d);
    for (let r = 0; r < d; r++) {
        let mx = findMaxi(m, r);
        if (mx !== r) {
            addRow(m, r, mx);
            addRow(res, r, mx);
        }
        let sc = m[r][r];
        scaleRow(m, r, 1.0 / sc);
        scaleRow(res, r, 1.0 / sc);
        for (let rr = 0; rr < d; rr++) {
            if (rr === r) continue;
            sc = m[rr][r];
            addScaledRow(m, rr, r, -sc);
            addScaledRow(res, rr, r, -sc);
        }
    }

    return $m4(res);
};


$m4.identity = function () {
    return $m4(
        [
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1],
        ]
    );

};

$m4.scale = function (sx, sy, sz) {
    return $m4(
        [
            [sx, 0, 0, 0],
            [0, sy, 0, 0],
            [0, 0, sz, 0],
            [0, 0, 0, 1],
        ]
    );
};


$m4.translation = function (tx, ty, tz) {
    return $m4(
        [
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [tx, ty, tz, 1],
        ]
    );
};

$m4.xRotation = function (angleInRadians) {
    let s = Math.sin(angleInRadians);
    let c = Math.cos(angleInRadians);
    return $m4(
        [
            [1, 0, 0, 0],
            [0, c, s, 0],
            [0, -s, c, 0],
            [0, 0, 0, 1],
        ]
    );
};

$m4.yRotation = function (angleInRadians) {
    let s = Math.sin(angleInRadians);
    let c = Math.cos(angleInRadians);
    return $m4(
        [
            [c, 0, -s, 0],
            [0, 1, 0, 0],
            [s, 0, c, 0],
            [0, 0, 0, 1],
        ]
    );
};

$m4.zRotation = function (angleInRadians) {
    let s = Math.sin(angleInRadians);
    let c = Math.cos(angleInRadians);
    return $m4(
        [
            [c, s, 0, 0],
            [-s, c, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1],
        ]
    );
};


$m4.shear3D = function (alpha, beta, gama) {
    let tga = Math.tan(degToRad(alpha));
    let tgb = Math.tan(degToRad(beta));
    let tgg = Math.tan(degToRad(gama));
    return $m4(
        [
            [1, tga, tga, 0],
            [tgb, 1, tgb, 0],
            [tgg, tgg, 1, 0],
            [0, 0, 0, 1],
        ]
    );
};


$m4.shear2D = function (alpha, beta) {
    let tga = Math.tan(degToRad(alpha));
    let tgb = Math.tan(degToRad(beta));
    return new $m4(
        [
            [1, tga, 0, 0],
            [tgb, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ]
    );
};

$m4.prototype.toFixedString = function (prec) {
    return $m4.toFixedString(this.elements, prec);
};


$m4.toFixedString = function (data, precision) {
    let prec = precision || 6;
    let res = [];
    data.forEach((x) => {
        let row = [];
        x.forEach((elem) => {
            let fixedVal = (elem.toFixed(prec));
            row.push(fixedVal);
        });
        res.push(row);
    });

    return $m4(res);
};

$m4.prototype.toFixed = function (prec) {
    return $m4.toFixed(this.elements, prec);
};

$m4.toFixed = function (data, precision) {
    let prec = precision || 6;
    let res = [];
    data.forEach((x) => {
        let row = [];
        x.forEach((elem) => {
            let fixedVal = (Number(elem.toFixed(prec)));
            row.push(fixedVal);
        });
        res.push(row);
    });

    return $m4(res);
};

$m4.fromOneDimArray = function (a) {
    if (a.length !== 16) {
        throw new Error("Cannot construct 4x4 matrix with array of length: " + a.length);
    }

    let data = [
        [a[0], a[1], a[2], a[3]],
        [a[4], a[5], a[6], a[7]],
        [a[8], a[9], a[10], a[11]],
        [a[12], a[13], a[14], a[15]]
    ];

    return $m4(data);
};

$m4.convertTo4 = function (data) {
    let ret = [];
    for (let c = 0; c < 3; c++) {
        if (c === 2) {
            ret.push([0, 0, 1, 0]);
        }
        let row = [];
        for (let k = 0; k < 3; k++) {
            row.push(data[k + c * 3]);
        }

        if (c !== 2) {
            row.push(0);
        } else {
            row.push(1);
        }

        ret.push(row);
    }

    return $m4(ret);
};

$m4.perspectiveMatrix = function (fov, aspect, near, far) {
    let f = Math.tan(Math.PI * 0.5 - 0.5 * fov);
    let rangeInv = 1.0 / (near - far);

    return $m4([
        [f / aspect, 0, 0, 0],
        [0, f, 0, 0],
        [0, 0, (near + far) * rangeInv, -1],
        [0, 0, near * far * rangeInv * 2, 0]
    ]);
};

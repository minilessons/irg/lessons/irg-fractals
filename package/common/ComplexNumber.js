/**
 * Represents an complex number.
 * @constructor creates complex number with real and imaginary part set to 0.0
 */
function ComplexNumber(real, imaginary) {
    let fu = new FrontendUtil();
    this.x = (real !== undefined) ? real : 0.0;
    this.y = (imaginary !== undefined) ? imaginary : 0.0;

    this.angle = function () {
        let angleFromNegativePiToPi = Math.atan2(this.y, this.x);

        if (angleFromNegativePiToPi < 0) {
            angleFromNegativePiToPi += (2 * Math.PI);
        }

        return angleFromNegativePiToPi;
    };

    this.nPow = function (exponent) {
        let c = new ComplexNumber(this.x, this.y);
        c.pow(exponent);
        return c;
    };

    this.pow = function (exponent) {
        let angle = this.angle();
        let modul = this.modul();
        let newModul = Math.pow(modul, exponent);
        let newAngle = fu.degToRad(exponent * angle);
        this.x = newModul * Math.cos(newAngle);
        this.y = newModul * Math.sin(newAngle);
    };

    this.modulSquared = function () {
        return this.x * this.x + this.y * this.y;
    };

    this.modul = function () {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    };

    this.toString = function () {
        let sign = this.y > 0 ? "+ " : "";
        return this.x + " " + sign + this.y + "i";
    };
}
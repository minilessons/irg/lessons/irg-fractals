/**
 * Common utility functions for frontend.
 * @returns {{convertHexToRGBA: (function(*, *=): RGBA), getGLColorFromHex: (function(*=, *=): *), hexToDecimal: (function(*=): number), RGBA: RGBA, degToRad: (function(*): number), radToDeg: (function(*): number), colors: {RED: RGBA, GREEN: RGBA, BLUE: RGBA, BLACK: RGBA, WHITE: RGBA, PURPLE: RGBA}}}
 * @constructor
 */
function FrontendUtil() {
    let colors = {
        RED: new RGBA(1.0, 0.0, 0.0, 1.0),
        GREEN: new RGBA(0.0, 1.0, 0.0, 1.0),
        BLUE: new RGBA(0.0, 0.0, 1.0, 1.0),
        BLACK: new RGBA(0.0, 0.0, 0.0, 1.0),
        WHITE: new RGBA(1.0, 1.0, 1.0, 1.0),
        PURPLE: new RGBA(1.0, 0.0, 1.0, 1.0)
    };


    /**
     * Specifies simple RGBA color values.
     *
     * @param red amount of red
     * @param green amount of green
     * @param blue amount of blue
     * @param alpha amount of transparency
     * @constructor
     */
    function RGBA(red, green, blue, alpha) {
        this.r = red;
        this.g = green;
        this.b = blue;
        this.a = alpha;

        this.cssString = function () {
            return "rgba(" + this.r + "," + this.g + "," + this.b + "," + this.a + ")"
        };
        this.normalize = function () {
            let maxC = 255;
            return new RGBA(this.r / maxC, this.g / maxC, this.b / maxC, this.a);
        };

        RGBA.prototype.asArray = function (color) {
            return [color.r, color.g, color.b, color.a];
        };

        this.asArray = function () {
            return RGBA.prototype.asArray(this);
        };

        this.asNormalizedArray = function () {
            return RGBA.prototype.asArray(this.normalize());
        };
    }


    /**
     * Transforms hex number (base 16) to decimal (base 10).
     *
     * @param hexNumber hex number to convert to decimal
     * @returns {Number} converted number
     */
    function hexToDecimal(hexNumber) {
        return parseInt(hexNumber, 16);
    }


    /**
     * Converts hexadecimal color to GL array of color attributes (rgba).
     * @param hexColor the hexadecimal color to convert
     * @param alpha the alpha factor of the hexadecimal color
     * @returns {*}
     */
    function getGLColorFromHex(hexColor, alpha) {
        let a = (alpha !== undefined) ? alpha : 1.0;
        return convertHexToRGBA(hexColor, a).asNormalizedArray();
    }


    /**
     * Converts hex color to rgba.
     *
     * @param hexColor color to convert
     * @param alpha alpha value for new rgba color
     * @returns {RGBA} rgba representation of hex color
     */
    function convertHexToRGBA(hexColor, alpha) {
        let a = (alpha === undefined) ? 1 : alpha;
        let red = hexToDecimal(hexColor.slice(1, 3));
        let green = hexToDecimal(hexColor.slice(3, 5));
        let blue = hexToDecimal(hexColor.slice(5, 7));
        return new RGBA(red, green, blue, a);
    }


    return {
        convertHexToRGBA: convertHexToRGBA,
        getGLColorFromHex: getGLColorFromHex,
        hexToDecimal: hexToDecimal,
        RGBA: RGBA,
        degToRad: degToRad,
        radToDeg: radToDeg,
        colors: colors,
    }
}


/**
 * Converts radians to degrees.
 * @param r angle in radians
 * @returns {number} angle in degrees
 */
function radToDeg(r) {
    return r * 180 / Math.PI;
}


/**
 * Converts degrees to radians.
 * @param d degree
 * @returns {number} angle in radians
 */
function degToRad(d) {
    return d * Math.PI / 180;
}


/**
 * Gets mouse coordinates for the specified canvas.
 * @param canvas the canvas at which mouse is
 * @param e event object
 * @returns {{x: (number|*), y: (number|*)}}
 */
function getMouse(canvas, e) {
    let rect = canvas.getBoundingClientRect();
    return {
        x: e.clientX - rect.left,
        y: e.clientY - rect.top
    };
}


function generateHexDigits(numberOfDigits) {
    let digits = "";
    let hexDigits = "ABCDEF0123456789";

    for (let c = 0; c < numberOfDigits; c++) {
        digits += hexDigits[Math.floor(Math.random() * hexDigits.length)];
    }
    return digits;
}


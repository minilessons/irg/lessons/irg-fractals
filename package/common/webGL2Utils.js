/**
 * Gets webGL2 for the specified canvas.
 *
 * @param canvas canvas to get webGL2 from
 * @returns {CanvasRenderingContext2D|WebGL2RenderingContext}
 */
function initWebGL2(canvas) {
    let gl = canvas.getContext('webgl2');
    if (!gl) {
        alert("Cannot initialize WebGL2. Your browser may not support it.");
    }

    return gl;
}


/**
 * Gets attribute location based on common prefix "a_".
 * @param gl the webGL context
 * @param program the program to get attribute location from
 * @param attributeName the name of the attribute without the prefix
 * @returns {number} the value of the location
 */
function getAttribLocation(gl, program, attributeName) {
    let shaderAttributePrefix = "a_";
    let loc = gl.getAttribLocation(program, shaderAttributePrefix + attributeName);
    if (loc < 0) {
        throw new Error("Cannot get location for name: " + attributeName);
    }

    return loc;
}


/**
 * Initializes shaders for webGL context
 *
 * @param gl the context to attach shaders to
 * @param vertexID the html id attribute value of a vertex shader
 * @param fragmentID the htmll id attribute value of a fragment shader
 * @param sourceChanger callback used for changing source before compilation
 * @returns {WebGLProgram} shader program
 */
function initShaders(gl, vertexID, fragmentID, sourceChanger) {
    let fragmentShader = getShader(gl, fragmentID, sourceChanger);
    let vertexShader = getShader(gl, vertexID, sourceChanger);

    if (fragmentShader === null || vertexShader === null) {
        throw new Error("Unable to init shaders!");
    }

    // Create shader program
    let shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        console.log("Unable to initialize the shader program: " + gl.getProgramInfoLog(shaderProgram));
        gl.deleteProgram(shaderProgram);
        throw new Error("Unable to init shaders!");
    }

    return shaderProgram;
}


/**
 * Gets shader from document, compiles it
 * @param gl openGL context
 * @param id shader script identification in document
 * @param sourceChanger callback used for changing source before compilation
 * @param type type of a shader to compile
 * @returns {*}
 */
function getShader(gl, id, sourceChanger, type) {
    let shaderScript, theSource, shader;
    shaderScript = document.getElementById(id);
    if (!shaderScript) {
        return null;
    }

    theSource = shaderScript.text;
    if (!type) {
        if (shaderScript.type === "x-shader/x-fragment") {
            type = gl.FRAGMENT_SHADER;
        } else if (shaderScript.type === "x-shader/x-vertex") {
            type = gl.VERTEX_SHADER;
        } else {
            // Unknown shader type
            return null;
        }
    }

    // Change the source if needed
    if (sourceChanger !== undefined) {
        theSource = sourceChanger(theSource, type);
    }

    shader = gl.createShader(type);
    gl.shaderSource(shader, theSource);
    gl.compileShader(shader);

    // Check for any compilation errors.
    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        console.log("An error occurred during shader compiling: ", gl.getShaderInfoLog(shader));
        gl.deleteShader(shader);
        return null;
    }

    return shader;
}
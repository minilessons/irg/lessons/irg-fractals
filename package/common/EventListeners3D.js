/**
 * Checks for any movement and sends needed data to camera move function.
 *
 * @param e the event object
 * @param state the state
 */
function eventMousemove3D(e, state) {
    if (state.animation.enabled === false) {
        return true;
    }

    let mouse = getMouse(state.canvas, e);

    // Calculate offset
    let props = state.animation;
    let yInv = state.canvas.height - mouse.y;
    let lastMouseInv = state.canvas.height - props.lastMouseY;

    let dx = mouse.x - props.lastMouseX;
    let dy = yInv - lastMouseInv;

    props.lastMouseX = mouse.x;
    props.lastMouseY = mouse.y;

    state.camera.processMouseMovement(dx, dy, true);
}


/**
 * Key down event dispatch function.
 * @param e the event object
 * @param state the state
 * @returns {boolean} true if animation was disabled in the state
 */
function eventKeyDown3D(e, state) {
    if (state.animation.enabled === false) {
        return true;
    }

    let x = e.which || event.keyCode;
    let code = state.constants.keyCodes;

    let successful = true;
    switch (x) {
        case code.escape:
            state.animation.enabled = false;
            break;
        case code.right:
            state.camera.processKeyboard(cameraMovementEnum.LOOK_RIGHT, state.animation.then);
            break;
        case code.D:
            state.camera.processKeyboard(cameraMovementEnum.RIGHT, state.animation.then);
            break;
        case code.left:
            state.camera.processKeyboard(cameraMovementEnum.LOOK_LEFT, state.animation.then);
            break;
        case code.A:
            state.camera.processKeyboard(cameraMovementEnum.LEFT, state.animation.then);
            break;
        case code.up:
            state.camera.processKeyboard(cameraMovementEnum.LOOK_UP, state.animation.then);
            break;
        case code.W:
            state.camera.processKeyboard(cameraMovementEnum.FORWARD, state.animation.then);
            break;
        case code.down:
            state.camera.processKeyboard(cameraMovementEnum.LOOK_DOWN, state.animation.then);
            break;
        case code.S:
            state.camera.processKeyboard(cameraMovementEnum.BACKWARD, state.animation.then);
            break;
        default:
            successful = false;
    }

    if (successful) {
        e.preventDefault();
    }
}


/**
 * Saves last mouse to the state animation.
 * @param e the event object
 * @param state the state
 */
function eventMouseDown3D(e, state) {
    let m = getMouse(state.canvas, e);
    state.animation.lastMouseX = m.x;
    state.animation.lastMouseY = m.y;
}


/**
 * The event handler for mouse wheel in 3D camera movements.
 * @param e the event object
 * @param state the state
 * @returns {boolean} true if the animation was disabled in the state
 */
function eventMouseWheel3D(e, state) {
    if (state.animation.enabled === false) {
        return true;
    }

    if (e.deltaY < 0) {
        state.camera.processMouseScroll(state.animation.scrollConst);
    } else {
        state.camera.processMouseScroll(-state.animation.scrollConst);
    }
}


/**
 * Creates a global "addWheelListener" method used as polyfill.
 * Example usage:
 *      addWheelListener(elem, function(e) {
 *             console.log(e.deltaY);
 *             e.preventDefault();
 *        });
 */
(function (window, document) {
    let prefix = "", _addEventListener, support;

    // detect event model
    if (window.addEventListener) {
        _addEventListener = "addEventListener";
    } else {
        _addEventListener = "attachEvent";
        prefix = "on";
    }

    // detect available wheel event
    support = "onwheel" in document.createElement("div") ? "wheel" : // Modern browsers support "wheel"
        document.onmousewheel !== undefined ? "mousewheel" : // Webkit and IE support at least "mousewheel"
            "DOMMouseScroll"; // let's assume that remaining browsers are older Firefox

    window.addWheelListener = function (elem, callback, useCapture) {
        _addWheelListener(elem, support, callback, useCapture);

        // handle MozMousePixelScroll in older Firefox
        if (support === "DOMMouseScroll") {
            _addWheelListener(elem, "MozMousePixelScroll", callback, useCapture);
        }
    };


    function _addWheelListener(elem, eventName, callback, useCapture) {
        elem[_addEventListener](prefix + eventName, support === "wheel" ? callback : function (originalEvent) {
            !originalEvent && (originalEvent = window.event);

            // create a normalized event object
            let event = {
                // keep a ref to the original event object
                originalEvent: originalEvent,
                target: originalEvent.target || originalEvent.srcElement,
                type: "wheel",
                deltaMode: originalEvent.type === "MozMousePixelScroll" ? 0 : 1,
                deltaX: 0,
                deltaY: 0,
                deltaZ: 0,
                preventDefault: function () {
                    originalEvent.preventDefault ?
                        originalEvent.preventDefault() :
                        originalEvent.returnValue = false;
                }
            };

            // calculate deltaY (and deltaX) according to the event
            if (support === "mousewheel") {
                event.deltaY = -1 / 40 * originalEvent.wheelDelta;
                // Webkit also support wheelDeltaX
                originalEvent.wheelDeltaX && (event.deltaX = -1 / 40 * originalEvent.wheelDeltaX);
            } else {
                event.deltaY = originalEvent.deltaY || originalEvent.detail;
            }

            // it's time to fire the callback
            return callback(event);

        }, useCapture || false);
    }

})(window, document);
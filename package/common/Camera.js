/**
 * Defines several possible options for camera movement.
 * Used as abstraction to stay away from window-system specific input methods.
 */
const cameraMovementEnum = {
    FORWARD: 1,
    BACKWARD: 2,
    LEFT: 3,
    RIGHT: 4,
    LOOK_UP: 5,
    LOOK_LEFT: 6,
    LOOK_RIGHT: 7,
    LOOK_DOWN: 8
};


/**
 * Represents a camera used to look at 3D scene.
 *
 * @param position the initial position vector for this camera
 * @param up the initial up vector for this camera
 * @param front the initial target vector for this camera
 * @constructor
 */
function Camera(position, up, front) {
    // Camera Attributes
    this.position = position || [5, 10, 0];
    this.front = front || [0, 0, -1];
    this.up = up || [0, 1, 0];
    this.right = [0, 0, 0];
    this.worldUp = [0, 1, 0];

    // RPG like mouse movements settings
    this.yaw = -90.0;
    this.pitch = 0.0;
    this.floor = 0;

    this.movementSpeed = 3;
    this.mouseSensitivity = 0.5;
    this.zoomSensitivity = 3.2;
    this.zoom = 45.0;

    let fu = new FrontendUtil();
    let myCam = this;

    this.setupCamera = function (position, up, yaw, pitch) {
        myCam.position = position;
        myCam.worldUp = up;
        myCam.yaw = yaw || -90.0;
        myCam.pitch = pitch || 0.0;

        myCam.updateCameraVectors();
    };

    this.getLookAt = function (cameraPosition, center, up) {
        let zAxis = $vec.normalize($vec.sub(cameraPosition, center));
        let xAxis = $vec.cross(up, zAxis);
        let yAxis = $vec.cross(zAxis, xAxis);

        return $m4([
            [xAxis[0], xAxis[1], xAxis[2], 0],
            [yAxis[0], yAxis[1], yAxis[2], 0],
            [zAxis[0], zAxis[1], zAxis[2], 0],
            [cameraPosition[0], cameraPosition[1], cameraPosition[2], 1]
        ]);
    };

    /**
     * Khronos look at matrix.
     *
     * @param eye the eye vector
     * @param center the center vector
     * @param up the up vector
     * @returns $m4 look at matrix of dimensions 4x4
     */
    this.lookAt = function (eye, center, up) {
        let f = $vec.normalize($vec.sub(center, eye));
        let s = $vec.normalize($vec.cross(f, $vec.normalize(up)));
        let u = $vec.cross(s, f);
        return $m4([
            [s[0], s[1], s[2], -eye[0] * s[0] - eye[1] * s[1] - eye[2] * s[2]],
            [u[0], u[1], u[2], -eye[0] * u[0] - eye[1] * u[1] - eye[2] * u[2]],
            [-f[0], -f[1], -f[2], eye[0] * f[0] + eye[1] * f[1] + eye[2] * f[2]],
            [0, 0, 0, 1]
        ]);
    };


    /**
     * Gets view matrix from the camera setup.
     */
    this.getViewMatrix = function () {
        let look = $vec.add(myCam.front, myCam.position);
        let cameraMatrix = myCam.getLookAt(myCam.position, look, myCam.up);

        return $m4.inv(cameraMatrix);
    };


    /**
     * Updates camera vectors from updated camera euler angles (yaw, pitch).
     */
    this.updateCameraVectors = function () {
        let front = [0, 0, 0];
        front[0] = Math.cos(fu.degToRad(myCam.yaw)) * Math.cos(fu.degToRad(myCam.pitch));
        front[1] = Math.sin(fu.degToRad(myCam.pitch));
        front[2] = Math.sin(fu.degToRad(myCam.yaw)) * Math.cos(fu.degToRad(myCam.pitch));

        myCam.front = $vec.normalize(front);
        myCam.right = $vec.normalize(($vec.cross(myCam.front, myCam.worldUp)));

        // Normalize the vectors, to avoid getting to 0 which has effect of slower movement.
        myCam.up = $vec.normalize($vec.cross(myCam.right, myCam.front));
    };


    /**
     * Keyboard movement event callback function which applies the movement to this camera.
     * @param direction the direction in which the camera should move
     * @param deltaTime time the time passed since last move
     */
    this.processKeyboard = function (direction, deltaTime) {
        // Check if direction was one of (W, S, A, D) movement.
        let velocity = myCam.movementSpeed * deltaTime;
        if (direction === cameraMovementEnum.FORWARD) {
            myCam.position = $vec.add(myCam.position, $vec.multiplyWithScalar(myCam.front, velocity));
        }
        if (direction === cameraMovementEnum.BACKWARD) {
            myCam.position = $vec.sub(myCam.position, $vec.multiplyWithScalar(myCam.front, velocity));
        }
        if (direction === cameraMovementEnum.LEFT) {
            myCam.position = $vec.sub(myCam.position, $vec.multiplyWithScalar(myCam.right, velocity));
        }
        if (direction === cameraMovementEnum.RIGHT) {
            myCam.position = $vec.add(myCam.position, $vec.multiplyWithScalar(myCam.right, velocity));
        }

        // Check if the direction was one of keyboard arrows (<-, ->, up, down)
        let xOffset = 10 * myCam.mouseSensitivity;
        let yOffset = 10 * myCam.mouseSensitivity;
        if (direction === cameraMovementEnum.LOOK_LEFT) {
            myCam.yaw += (-1 * xOffset);
        }
        if (direction === cameraMovementEnum.LOOK_RIGHT) {
            myCam.yaw += xOffset;
        }
        if (direction === cameraMovementEnum.LOOK_UP) {
            myCam.pitch += yOffset;
        }
        if (direction === cameraMovementEnum.LOOK_DOWN) {
            myCam.pitch += (-1 * yOffset);
        }
        if (myCam.position[1] <= myCam.floor) {
            myCam.position[1] = myCam.floor;
        }

        myCam.updateCameraVectors();
    };

    /**
     * Mouse movement event callback function which updates camera look vectors based on mouse offset.
     * @param xOffset the x offset
     * @param yOffset the y offset
     * @param constrainPitch constraint to use when dealing with out of allowed bounds for pitch (avoids flipping screen)
     */
    this.processMouseMovement = function (xOffset, yOffset, constrainPitch) {
        myCam.yaw += (xOffset * myCam.mouseSensitivity);
        myCam.pitch += (yOffset * myCam.mouseSensitivity);

        // Constrain the pitch to 90 and -90 (no screen flipping)
        if (constrainPitch) {
            if (myCam.pitch > 89.0) {
                myCam.pitch = 89.0;
            }
            if (myCam.pitch < -89.0) {
                myCam.pitch = -89.0;
            }
        }

        myCam.updateCameraVectors();
    };


    /**
     * Processes input received from a mouse scroll-wheel event.
     * Only requires input on the vertical wheel-axis
     * @param yOffset the offset of the mouse wheel
     */
    this.processMouseScroll = function (yOffset) {
        if (myCam.zoom >= 45.0 && myCam.zoom <= 180.0) {
            myCam.zoom -= (yOffset * myCam.zoomSensitivity);
        }

        if (myCam.zoom <= 45.0) {
            myCam.zoom = 45.0;
        }

        if (myCam.zoom >= 170.0) {
            myCam.zoom = 170.0;
        }
    };

    // Update camera vectors with iniitial setup.
    myCam.updateCameraVectors();
}
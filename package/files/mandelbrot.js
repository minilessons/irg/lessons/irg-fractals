function MandelbrotRenderer(canvas) {
    let frontendUtil = new FrontendUtil();
    let canvasWidth = 600;
    let canvasHeight = 600;
    this.constants = getConstants();

    // Listeners setup
    this.animation = {
        then: 5,
        lastMouseX: 0,
        lastMouseY: 0,
        scrollConst: 1.2,
        rotationSpeed: 20,
        enabled: false
    };

    let mandelbrotConfig = {
        defaultZoomScale: 2,
        // Raster coordinate system
        xMin: 0.0,
        xMax: canvasWidth,
        yMin: 0.0,
        yMax: canvasHeight,

        // Complex coordinate system
        uMin: -2.0,
        uMax: 1.0,
        vMin: -1.2,
        vMax: 1.2,
        depthLimit: 1000,
        colorSheme: COLOR_SHEMES.BLACK_WHITE,
        exponent: 2
    };

    this.fractalConfig = mandelbrotConfig;

    // Init webGL
    let gl = initWebGL(canvas);
    initWebGLSettings(gl);

    let program = initShaders(gl, "mandelbrot-vertex-shader", "mandelbrot-fragment-shader");

    // Setup locations
    let a_position = getAttribLocation(gl, program, "position");
    let mandelbrotObj = {
        location: a_position,
        numOfCoordinates: 2,
        normalize: false,
        bufferData: getRasterVertices(mandelbrotConfig)
    };


    function updateCanvas() {
        myState.canvas.width = myState.fractalConfig.xMax;
        myState.canvas.height = myState.fractalConfig.yMax;
    }


    this.signalRendering = function (rendering) {
        myState.canvas.className = (rendering) ? "canvas-active" : "canvas-passive";
    };

    this.updateLater = function (afterFinish) {
        myState.signalRendering(true);
        window.setTimeout(function () {
            myState.invalidate();
            if (afterFinish) {
                afterFinish();
            }
            myState.signalRendering(false);
        }, 100);
    };

    /**
     * Draws the scene.
     */
    this.invalidate = function () {
        updateCanvas();
        updateVertexBuffer(gl, mandelbrotObj, mandelbrotConfig);

        if (myState.recompileShaders) {
            recompileShaders();
            myState.recompileShaders = false;
        }
        drawScene(gl);
    };


    function recompileShaders() {
        let shaders = gl.getAttachedShaders(program);
        gl.detachShader(program, shaders[0]);
        gl.detachShader(program, shaders[1]);
        updateShaders(gl, program, "mandelbrot-vertex-shader", "mandelbrot-fragment-shader");
    }


    /**
     * The render function which draws the scene.
     * @param gl the WebG: context to draw the scene to
     */
    function drawScene(gl) {
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

        // Clear canvas
        gl.clear(gl.COLOR_BUFFER_BIT);

        // Use program
        gl.useProgram(program);

        // Set uniforms
        gl.uniform1i(gl.getUniformLocation(program, "color_sheme"), mandelbrotConfig.colorSheme);
        gl.uniform1f(gl.getUniformLocation(program, "xMin"), mandelbrotConfig.xMin);
        gl.uniform1f(gl.getUniformLocation(program, "xMax"), mandelbrotConfig.xMax);
        gl.uniform1f(gl.getUniformLocation(program, "yMin"), mandelbrotConfig.yMin);
        gl.uniform1f(gl.getUniformLocation(program, "yMax"), mandelbrotConfig.xMax);

        gl.uniform1f(gl.getUniformLocation(program, "uMin"), mandelbrotConfig.uMin);
        gl.uniform1f(gl.getUniformLocation(program, "uMax"), mandelbrotConfig.uMax);
        gl.uniform1f(gl.getUniformLocation(program, "vMin"), mandelbrotConfig.vMin);
        gl.uniform1f(gl.getUniformLocation(program, "vMax"), mandelbrotConfig.vMax);
        gl.uniform1i(gl.getUniformLocation(program, "limit"), mandelbrotConfig.depthLimit);
        gl.uniform1f(gl.getUniformLocation(program, "exponent"), mandelbrotConfig.exponent);

        enableAttrib(gl, mandelbrotObj);
        let numOfPoints = mandelbrotObj.bufferData.length / 2;
        gl.drawArrays(gl.POINTS, 0, numOfPoints);
    }


    /**
     * Initializes common webGL settings like clear color.
     * @param gl the webGL context to initialize settings for
     */
    function initWebGLSettings(gl) {
        let c = frontendUtil.getGLColorFromHex("#64cfa5", 1.0);
        gl.clearColor(c[0], c[1], c[2], c[3]);
    }


    function updateCoordinates(m) {
        let complexPoint = getComplexPointFromRaster(m.x, m.y);
        myState.complexX.innerHTML = complexPoint.x.toFixed(4);
        myState.complexY.innerHTML = complexPoint.y.toFixed(4);
        myState.rasterX.innerHTML = m.x.toFixed(4);
        myState.rasterY.innerHTML = m.y.toFixed(4);
    }


    /**
     * Helper function which adds all needed event listeners.
     */
    function addEventListeners() {
        function keyDownCallback(e) {
            eventKeyDown3D(e, myState);
            myState.invalidate();
        }


        function containedIn(x, y, w, h, m) {
            let mx = m.x;
            let my = m.y;
            return (mx > x) && (mx < (x + w)) &&
                (my > y) && (my <= (y + h));
        }


        function windowMouseDown(e) {
            let m = getMouse(myState.canvas, e);
            let c = myState.canvas;
            let r = c.getBoundingClientRect();

            myState.animation.enabled = containedIn(0, 0, r.width, r.height, m);
            return true;
        }


        function mouseDownCallback(e) {
            eventMouseDown3D(e, myState);
            if (myState.animation.enabled) {
                let x = myState.animation.lastMouseX;
                let y = myState.animation.lastMouseY;

                if (myState.config.zoomWithClickEnabled) {
                    zoomWith(getComplexPointFromRaster(x, myState.canvas.height - y), myState.fractalConfig.defaultZoomScale, myState);
                }
            }
            myState.updateLater()
        }


        function mouseMoveCallback(e) {
            let m = getMouse(myState.canvas, e);
            m.y = myState.canvas.height - m.y;
            updateCoordinates(m);
        }


        function wheelMoveCallback(e) {
            eventMouseDown3D(e, myState);
            let x = myState.animation.lastMouseX;
            let y = myState.animation.lastMouseY;

            let defaultScale = myState.fractalConfig.defaultZoomScale;
            let scale = Math.sign(e.deltaY) === -1 ? defaultScale : 1 / defaultScale;
            zoomWith(getComplexPointFromRaster(x, myState.canvas.height - y), scale, myState);

            e.preventDefault();
            myState.updateLater();
        }


        let can = gl.canvas;
        can.addEventListener("mousedown", mouseDownCallback, false);
        can.addEventListener("mousemove", mouseMoveCallback, false);
        window.addEventListener("mousedown", windowMouseDown, true);
        addWheelListener(can, wheelMoveCallback);
    }


    /**
     * Converts a raster coordinate to the complex plane coordinates.
     * @param x the x raster coordintae
     * @param y the y raster coordinate
     * @returns {{x: *, y: *}} the complex point converted from raster coordinates
     */
    function getComplexPointFromRaster(x, y) {
        let xW = mandelbrotConfig.xMax - mandelbrotConfig.xMin;
        let yH = mandelbrotConfig.yMax - mandelbrotConfig.yMin;
        let uW = mandelbrotConfig.uMax - mandelbrotConfig.uMin;
        let vH = mandelbrotConfig.vMax - mandelbrotConfig.vMin;
        let cx = (x - mandelbrotConfig.xMin) / (xW) * (uW) + mandelbrotConfig.uMin;
        let cy = (y - mandelbrotConfig.yMin) / (yH) * (vH) + mandelbrotConfig.vMin;

        return {
            x: cx,
            y: cy
        }
    }


    let myState = this;
    myState.canvas = canvas;

    myState.rasterX = document.getElementById("raster-x");
    myState.rasterY = document.getElementById("raster-y");
    myState.complexX = document.getElementById("complex-x");
    myState.complexY = document.getElementById("complex-y");

    addEventListeners();
    updateCanvas();
    initWebGLBuffer(gl, mandelbrotObj);
    enableAttrib(gl, mandelbrotObj);

    return myState;
}

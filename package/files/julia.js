function JuliaRenderer(canvas, initialW, initialH) {
    let frontendUtil = new FrontendUtil();
    let canvasWidth = initialW || 300;
    let canvasHeight = initialH || 300;
    this.constants = getConstants();

    // Listeners setup
    this.animation = {
        then: 5,
        lastMouseX: 0,
        lastMouseY: 0,
        scrollConst: 1.2,
        rotationSpeed: 20,
        enabled: false
    };


    let juliaConfig = {
        defaultZoomScale: 2,
        // Raster coordinate system
        xMin: 0.0,
        xMax: canvasWidth,
        yMin: 0.0,
        yMax: canvasHeight,

        // Complex coordinate system
        uMin: -1.5,
        uMax: 1.0,
        vMin: -1.0,
        vMax: 1.0,
        depthLimit: 2 * 1000,
        exponent: 2.0,
        c: new ComplexNumber(-0.67, 0.34),
        colorSheme: COLOR_SHEMES.BLACK_WHITE
    };


    this.fractalConfig = juliaConfig;

    // Init Canvas properties
    canvas.width = canvasWidth;
    canvas.height = canvasHeight;

    // Init webGL
    let gl = initWebGL(canvas);
    initWebGLSettings(gl);

    // Init shaders
    let program = initShaders(gl, "julia-vertex-shader", "julia-fragment-shader");

    // Setup locations
    let a_position = getAttribLocation(gl, program, "position");
    let juliaObj = {
        location: a_position,
        numOfCoordinates: 2,
        normalize: false,
        bufferData: getRasterVertices(juliaConfig)
    };

    /**
     * Draws the scene.
     */
    this.invalidate = function () {
        updateCanvas();
        updateVertexBuffer(gl, juliaObj, juliaConfig);

        if (myState.recompileShaders) {
            recompileShaders();
            myState.recompileShaders = false;
        }
        drawScene(gl);
    };


    function updateCanvas() {
        myState.canvas.width = myState.fractalConfig.xMax;
        myState.canvas.height = myState.fractalConfig.yMax;
    }


    function recompileShaders() {
        let shaders = gl.getAttachedShaders(program);
        gl.detachShader(program, shaders[0]);
        gl.detachShader(program, shaders[1]);
        updateShaders(gl, program, "julia-vertex-shader", "julia-fragment-shader");
    }


    this.setInitialPoint = function (complexNumber) {
        juliaConfig.c = complexNumber;
    };

    this.setColorSheme = function (colorSheme) {
        juliaConfig.colorSheme = colorSheme;
    };


    /**
     * The render function which draws the scene.
     * @param gl the WebG: context to draw the scene to
     */
    function drawScene(gl) {
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

        // Clear canvas
        gl.clear(gl.COLOR_BUFFER_BIT);

        // Use program
        gl.useProgram(program);

        // Set uniforms
        gl.uniform1i(gl.getUniformLocation(program, "color_sheme"), juliaConfig.colorSheme);
        gl.uniform1f(gl.getUniformLocation(program, "xMin"), juliaConfig.xMin);
        gl.uniform1f(gl.getUniformLocation(program, "xMax"), juliaConfig.xMax);
        gl.uniform1f(gl.getUniformLocation(program, "yMin"), juliaConfig.yMin);
        gl.uniform1f(gl.getUniformLocation(program, "yMax"), juliaConfig.xMax);

        gl.uniform1f(gl.getUniformLocation(program, "uMin"), juliaConfig.uMin);
        gl.uniform1f(gl.getUniformLocation(program, "uMax"), juliaConfig.uMax);
        gl.uniform1f(gl.getUniformLocation(program, "vMin"), juliaConfig.vMin);
        gl.uniform1f(gl.getUniformLocation(program, "vMax"), juliaConfig.vMax);
        gl.uniform2f(gl.getUniformLocation(program, "initial_complex_number"), juliaConfig.c.x, juliaConfig.c.y);
        gl.uniform1f(gl.getUniformLocation(program, "exponent"), juliaConfig.exponent);
        gl.uniform1i(gl.getUniformLocation(program, "limit"), juliaConfig.depthLimit);


        enableAttrib(gl, juliaObj);
        let numOfPoints = juliaObj.bufferData.length / 2;
        gl.drawArrays(gl.POINTS, 0, numOfPoints);
    }


    /**
     * Initializes common webGL settings like clear color.
     * @param gl the webGL context to initialize settings for
     */
    function initWebGLSettings(gl) {
        let c = frontendUtil.getGLColorFromHex("#fef9ff", 1.0);
        gl.clearColor(c[0], c[1], c[2], c[3]);
    }


    this.signalRendering = function (rendering) {
        myState.canvas.className = (rendering) ? "canvas-active" : "canvas-passive";
    };

    this.updateLater = function (afterFinish) {
        myState.signalRendering(true);
        window.setTimeout(function () {
            myState.invalidate();
            if (afterFinish) {
                afterFinish();
            }
            myState.signalRendering(false);
        }, 100);
    };


    this.updateCoordinates = function (m) {
        let complexPoint = getComplexPointFromRaster(m.x, m.y);
        myState.complexX.innerHTML = complexPoint.x.toFixed(4);
        myState.complexY.innerHTML = complexPoint.y.toFixed(4);
        myState.rasterX.innerHTML = m.x.toFixed(4);
        myState.rasterY.innerHTML = m.y.toFixed(4);
    };


    /**
     * Converts a raster coordinate to the complex plane coordinates.
     * @param x the x raster coordinate
     * @param y the y raster coordinate
     * @returns {{x: *, y: *}} the complex point converted from raster coordinates
     */
    function getComplexPointFromRaster(x, y) {
        let xW = juliaConfig.xMax - juliaConfig.xMin;
        let yH = juliaConfig.yMax - juliaConfig.yMin;
        let uW = juliaConfig.uMax - juliaConfig.uMin;
        let vH = juliaConfig.vMax - juliaConfig.vMin;
        let cx = (x - juliaConfig.xMin) / (xW) * (uW) + juliaConfig.uMin;
        let cy = (y - juliaConfig.yMin) / (yH) * (vH) + juliaConfig.vMin;

        return {
            x: cx,
            y: cy
        }
    }


    let myState = this;
    myState.canvas = canvas;

    myState.complexX = document.getElementById("complex-x");
    myState.complexY = document.getElementById("complex-y");
    myState.rasterX = document.getElementById("raster-x");
    myState.rasterY = document.getElementById("raster-y");

    updateCanvas();
    initWebGLBuffer(gl, juliaObj);
    enableAttrib(gl, juliaObj);

    return myState;
}
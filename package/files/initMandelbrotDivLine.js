(function init() {
    function initMandelbrot() {
        let suffixID = "-m";
        let canvasM = document.getElementById("mandelbrot_canvas");

        let canvasWidth = 600;
        let canvasHeight = 600;
        let config = {
            // Translations
            convergesText: "Da",
            divergesText: "Ne",

            // Toolbox
            hasUiToolbox: false,
            showCoords: true,

            gridStep: 30, // Raster; Pixels
            gridPointSize: 1.0,
            gridTickSize: 15, // Raster; Pixels
            gridTickTextOffset: 10, // Raster; Pixels
            showGrid: true,
            // gridColorHex: "#717f85",
            // gridColorHex: "#88e663",

            getColorMode: function (cnf) {
                if (cnf.colorSheme !== COLOR_SHEMES.GRAY) {
                    return "#FFFFFF" // white
                } else {
                    return "#002233" // black
                }
            },

            mandelbrotNeedUpdate: true,
            divLineNeedUpdate: true,
            userConfig: {
                defaultZoomScale: 1.5,
                // Raster coordinate system
                xMin: 0.0,
                xMax: canvasWidth,
                yMin: 0.0,
                yMax: canvasHeight,

                // Complex coordinate system
                zoomLocked: true,
                uMin: -2.3,
                uMax: 2.3,
                vMin: -2.3,
                vMax: 2.3,
                limits: {
                    min: 1,
                    max: 2000, // Was 100 * 1000; 100k
                    stepMult: 2,
                    step: 2,
                    default: 1,
                    currDepthLimit: 1
                },
                hidden: true,
                colorSheme: COLOR_SHEMES.GRAY,
                exponent: 2
            },

            divergenceLine: {
                pointColorHex: "#000000",
                lineColorHex: "#FF0000",
                pointSize: 4.0,
                hidden: true,
                initialPoint: new ComplexNumber(), // (0.0, 0.0)
                lastMovePoint: new ComplexNumber(1, 1),
                converges: false,

                convIterLimit: 256,
                stopThreshold: 200,

                pointsOnly: false
            },
            zoomOutComplexXMax: 25,
            zoomOutComplexYMax: 20,
            zoomWithClickEnabled: false,
        };
        let mandelbrotRenderer = new WebGL2MandelbrotRenderer(canvasM, config);

        // Add listeners to div line canvas, but actual logic is performed on Mandelbrot canvas
        addEventListenersMandelbrot(mandelbrotRenderer, mandelbrotRenderer.canvasDivLine);

        // Add key down listeners
        // For depth limit divergence changes
        mandelbrotRenderer.canvasDivLine.addEventListener("keyup", (e) => keyUpCallback(e, mandelbrotRenderer), false);
        mandelbrotRenderer.canvasDivLine.addEventListener("keyup", (e) => keyUpCallbackCustom(e, mandelbrotRenderer), false);
        updateDivergenceDepthStatus(mandelbrotRenderer.fractalConfig.limits.currDepthLimit);

        function updateDivergenceDepthStatus(newDepth) {
            let divDepth = document.getElementById("divergence_depth");
            if (divDepth) {
                divDepth.innerHTML = "" + newDepth;
            }
        }

        function keyUpCallbackCustom(e, state) {
            let x = e.which || event.keyCode;
            let code = state.constants.keyCodes;
            switch (x) {
                case code.I:
                case code.numpadPlus:
                    // Update status
                    updateDivergenceDepthStatus(state.fractalConfig.limits.currDepthLimit)
                    break;
                case code.D:
                case code.numpadMinus:
                    updateDivergenceDepthStatus(state.fractalConfig.limits.currDepthLimit)
                    break;
            }
        }

        // Add another mouse down listener but for right mouse click which will update julia canvas
        // canvasM.addEventListener("mousedown", mouseDownJuliaCallback, false);
        mandelbrotRenderer.canvasDivLine.addEventListener("mousedown", (e) => mouseDownCallback(e, mandelbrotRenderer, mandelbrotRenderer.canvasDivLine), false);
        // canvasM.addEventListener("mousemove", mouseMoveCallback, false);
        mandelbrotRenderer.canvasDivLine.addEventListener("mousemove", (e) => mouseMoveCallback(e, mandelbrotRenderer, mandelbrotRenderer.canvasDivLine), false);
        // canvasM.addEventListener("mouseup", mouseUpCallback, false);
        mandelbrotRenderer.canvasDivLine.addEventListener("mouseup", (e) => mouseUpCallback(e, mandelbrotRenderer), false);
        // Disable on canvas
        disableContextMenu(mandelbrotRenderer.canvasDivLine);
        mandelbrotRenderer.updateLater();


        function mouseMoveCallback(e, state, canvas) {
            // Mandelbrot update for divergence line
            if (mandelbrotRenderer.animation.divLineDragging) {
                updateDivLine(e, state, canvas);
                mandelbrotRenderer.config.divLineNeedUpdate = true;
                mandelbrotRenderer.invalidate();
            }
        }

        function updateDivLine(e, state, canvas) {
            let mouse = getMouse(canvas, e);
            let x = mouse.x;
            let y = canvas.height - mouse.y;
            // To complex converted later, once divergence test is called
            // let complexPoint = getComplexPointFromRaster(state.fractalConfig, x, y);
            state.config.divergenceLine.lastMovePoint = { x: x, y: y }; // new ComplexNumber(complexPoint.x, complexPoint.y);
        }

        function mouseUpCallback(e, state) {
            state.animation.dragging = false;
            state.animation.divLineDragging = false;
        }

        function mouseDownCallback(e, state, canvas) {
            // Mandelbrot setup
            if (isLeftClick(e)) {
                state.animation.divLineDragging = true;
                state.config.divergenceLine.hidden = false;
                updateDivLine(e, state, canvas);
                mandelbrotRenderer.config.divLineNeedUpdate = true;
                mandelbrotRenderer.invalidate();
            }
        }
    }

    initMandelbrot();
})();
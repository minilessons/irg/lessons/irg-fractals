// ==========================================================
//    Flames utility functions
// ==========================================================
function createAffineTransform(t) {
    return {
        a: t[0], b: t[1], c: t[2], d: t[3], e: t[4], f: t[5]
    };
}


function truncate(x) {
    if (Math.trunc && Math.trunc !== undefined) {
        return Math.trunc(x);
    }
    return x > 0 ? Math.floor(x) : Math.ceil(x);
}


function psi() {
    return Math.random();
}


function lambda() {
    if (Math.random() > 0.5) {
        return -1
    } else {
        return 1;
    }
}


function omega() {
    if (Math.random() > 0.5) {
        return Math.PI;
    } else {
        return 0;
    }
}


function phi(x, y) {
    if (x === 0) {
        return 0;
    }
    let val = Math.atan(y / x);
    if (val === undefined) {
        throw new Error("Arcus tagnent cannot be calculated for:" + x + " " + y);
    }
    return val;
}


function theta(x, y) {
    if (y === 0) {
        return 0;
    }
    let val = Math.atan(x / y);
    if (val === undefined || Number.isNaN(val)) {
        throw new Error("Arcus tagnent cannot be calculated for:" + x + " " + y);
    }
    return val;
}


function complexModul(x, y) {
    return Math.sqrt(x * x + y * y);
}

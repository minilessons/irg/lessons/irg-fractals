function WebGL2MandelbrotRenderer(canvasMandelbrot, mandelbrotConfig) {
    let frontendUtil = new FrontendUtil();
    this.constants = getConstants();

    // Div Line setup
    let canvasDivLine = document.getElementById("div_line_canvas");
    let glDivLine = initWebGL2(canvasDivLine);
    initWebGLSettings(glDivLine, "#000000", 0.0);

    // Listeners setup
    this.animation = {
        then: 5,
        lastMouseX: 0,
        lastMouseY: 0,
        scrollConst: 1.2,
        rotationSpeed: 20,
        enabled: false,

        divLineDragging: false
    };

    this.config = mandelbrotConfig;
    this.fractalConfig = mandelbrotConfig.userConfig;

    // Init webGL
    let glMandelbrot = initWebGL2(canvasMandelbrot);
    // initWebGLSettings(glMandelbrot, "#64cfa5", 1.0);
    initWebGLSettings(glMandelbrot, "#FFFFFF", 1.0);

    let programMandelbrot = initShaders(glMandelbrot, "mandelbrot-vertex-shader", "mandelbrot-fragment-shader");
    let programDivLine = initShaders(glDivLine, "div-line-vertex-shader", "div-line-fragment-shader");

    let a_position = getAttribLocation(glMandelbrot, programMandelbrot, "position");
    let mandelbrotObj = {
        location: a_position,
        numOfCoordinates: 2,
        normalize: false,
        bufferData: [],
        vao: undefined
    };

    // Setup locations
    let a_positionDivLine = getAttribLocation(glDivLine, programDivLine, "position");
    let divergenceLineObj = {
        location: a_positionDivLine,
        numOfCoordinates: 2,
        normalize: false,
        bufferData: [],
        vao: undefined
    };

    // Coordinate grid setup
    // Overlay text canvas
    let gridTextCanvas = document.getElementById("text_grid_canvas");
    let gridCtx = gridTextCanvas.getContext("2d");


    // Program setup
    let programGrid = initShaders(glMandelbrot, "grid-vertex-shader", "grid-fragment-shader");
    let a_positionGrid = getAttribLocation(glMandelbrot, programGrid, "position");
    let gridObj = {
        location: a_positionGrid,
        numOfCoordinates: 2,
        normalize: false,
        bufferData: [],
        vao: undefined
    };

    function initGridLines(obj, initGrid = true) {
        if (obj && initGrid) {
            let points = getCoordinateArray(glMandelbrot.canvas, myState.config.gridStep, myState.config.gridTickSize);
            obj.bufferData = [];
            for (let point of points) {
                obj.bufferData.push(point);
            }
        }

        // Draw overlay text
        clearCtx(gridCtx);
        if (myState.config.showGrid) {
            drawGridOverlayText(gridCtx, myState.config.gridStep, myState.config.gridTickSize + myState.config.gridTickTextOffset);
        }
        if (myState.config.showCoords) {
            drawCurrentCoords(gridCtx, myState.coords);
        }
    }

    function drawCurrentCoords(ctx, coords) {
        let text = `(${coords.complexX.toFixed(4)}, ${coords.complexY.toFixed(4)})`;
        ctx.beginPath();

        let fourthQuadrant = ((coords.compleX < 0) && (coords.complexY < 0));
        let leftOff = coords.complexX || fourthQuadrant > 0.0 ? 8 : 0;
        let rightOff = 0;
        let upOff = 0;
        let downOff = fourthQuadrant ? -20 : 0;
        ctx.fillText(text, coords.rasterX + leftOff + rightOff, coords.rasterY + upOff + downOff);
    }

    function clearCtx(ctx) {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    }


    function drawGridOverlayText(ctx, step, tickLength) {
        ctx.font = "9px Arial";
        ctx.strokeStyle = myState.config.getColorMode(myState.fractalConfig);
        ctx.fillStyle = myState.config.getColorMode(myState.fractalConfig);
        ctx.beginPath();

        let origin = calcOrigin(ctx.canvas, step);
        let verEnd = ctx.canvas.height;
        let horEnd = ctx.canvas.width;
        origin.y = verEnd - origin.y;
        // let halfTickSize = Math.floor(tickLength / 2);
        for (let c = 0; c < (horEnd + step); c += step) {
            if (origin.x == c) {
                // Do it only here
                let complexPlaneValue = getComplexPointFromRaster(myState.fractalConfig, c, origin.y)
                let text = toFixed(complexPlaneValue.x, 0);
                ctx.fillText(text, c + 7, origin.y + tickLength / 1.4);
                continue;
            }
            // y not needed, so we just send c also
            let complexPlaneValue = getComplexPointFromRaster(myState.fractalConfig, c, origin.y)
            let leftOffset = complexPlaneValue.x < 0 ? 9 : 7;
            let text = toFixed(complexPlaneValue.x);
            ctx.fillText(text, c - leftOffset, origin.y + tickLength);
        }

        for (let y = 0; y < (step + verEnd); y += step) {
            if (origin.y == y) {
                // Skip;
                continue;
            }

            let complexPlaneValue = getComplexPointFromRaster(myState.fractalConfig, origin.x, verEnd - y)
            let text = toFixed(complexPlaneValue.y);
            let leftOffset = complexPlaneValue.y > 0 ? tickLength / 1.3 : tickLength / 1.1;
            ctx.fillText(text, origin.x - leftOffset, y + tickLength / 1.6);
        }
    }

    function getIntermediateMandelbrotPoints(z0, pointRaster) {
        let c = getComplexPointFromRaster(myState.fractalConfig, pointRaster.x, pointRaster.y);
        let data = divergenceTestWithLog(z0, c, myState.config.divergenceLine.convIterLimit, myState.config.divergenceLine.stopThreshold);
        let converges = data.status === -1;
        myState.config.divergenceLine.converges = converges;

        // Update status
        let divergenceStatusEl = document.getElementById("divergence_status");
        if(divergenceStatusEl) {
            divergenceStatusEl.innerHTML = converges ? myState.config.convergesText : myState.config.divergesText;
        }

        return data.values;
    }

    function initPointsDivLine(divergenceLineObj) {
        let ip = myState.config.divergenceLine.initialPoint;
        let sp = myState.config.divergenceLine.lastMovePoint;
        let frConfig = myState.fractalConfig;

        // Calculate all intermediate points upon some limit
        let points = getIntermediateMandelbrotPoints(ip, sp);
        divergenceLineObj.bufferData = [];
        for (let point of points) {
            // Use David line algorithms!
            // Convert to raster and then in vertex to clip, or convert to clip space immediatelly (used)
            let clipX = affineTransformABtoCD(point.x, frConfig.uMin, frConfig.uMax, -1, 1) // clip space transform
            let clipY = affineTransformABtoCD(point.y, frConfig.vMin, frConfig.vMax, -1, 1) // clip space transform

            divergenceLineObj.bufferData.push(clipX);
            divergenceLineObj.bufferData.push(clipY);
        }
    }


    function initTriangles(mandelbrotObj) {
        // let o = 1.0;
        mandelbrotObj.bufferData = [].concat(
            /*triangulateRectangle(-1.0, -1.0, o, o),
            triangulateRectangle(-1.0, 0, o, o),
            triangulateRectangle(0, -1.0, o, o),
            triangulateRectangle(0, 0, o, o),*/
            // This is only calculated, which is ok in general, but when scale changes we should update?
            triangulateRectangle(-1.0, -1.0, 2.0, 2.0),
        );
    }


    function updateCanvas(canvas) {
        canvas.width = myState.fractalConfig.xMax;
        canvas.height = myState.fractalConfig.yMax;
    }

    this.signalRendering = function (rendering) {
        // Canvas signal rendering for mandelbrot canvas
        myState.canvas.className = (rendering) ? "canvas-active" : "canvas-passive";
    };

    this.updateLater = function (afterFinish) {
        myState.signalRendering(true);
        window.setTimeout(function () {
            myState.invalidate();
            if (afterFinish) {
                afterFinish();
            }
            myState.signalRendering(false);
        }, 100);
    };

    /**
     * Draws the scene.
     */
    this.invalidate = function () {
        updateCanvas(canvasMandelbrot);
        updateCanvas(canvasDivLine);

        if (myState.recompileShaders) {
            recompileShaders(glMandelbrot, program, "mandelbrot-vertex-shader", "mandelbrot-fragment-shader");
            recompileShaders(glMandelbrot, programGrid, "grid-vertex-shader", "grid-fragment-shader");
            recompileShaders(glDivLine, programDivLine, "div-line-vertex-shader", "div-line-fragment-shader");
            myState.recompileShaders = false;
        }

        if (myState.config.mandelbrotNeedUpdate || myState.config.updateAll) {
            initTriangles(mandelbrotObj);
            initWebGLVAOBuffer(glMandelbrot, mandelbrotObj);
            renderMandelbrot(glMandelbrot, programMandelbrot);

            myState.config.mandelbrotNeedUpdate = false;
        }

        if (myState.config.divLineNeedUpdate || myState.config.updateAll) {
            initPointsDivLine(divergenceLineObj);
            initWebGLVAOBuffer(glDivLine, divergenceLineObj);
            renderDivergenceLine(glDivLine, programDivLine);

            myState.config.divLineNeedUpdate = false;
        }

        // Grid update
        if (myState.config.updateAll) {
            initGridLines(gridObj);
        }

        myState.config.updateAll = false;
    };


    function recompileShaders(gl, program, vertId, fragId) {
        let shaders = gl.getAttachedShaders(program);
        gl.detachShader(program, shaders[0]);
        gl.detachShader(program, shaders[1]);
        updateShaders(gl, program, vertId, fragId);
    }


    /**
     * The render function which draws the scene.
     * @param gl the WebG: context to draw the scene to
     */
    function renderMandelbrot(gl, program) {
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

        // Clear canvas
        gl.clear(gl.COLOR_BUFFER_BIT);

        if (!myState.fractalConfig.hidden) {
            drawMandelbrot(gl, program);
        }

        // Render coordinate grid (no hidden, always show - but could hide it)
        if (myState.config.showGrid) {
            drawGridLines(gl, programGrid);
        } else {
            // Remove grid overlay text
            clearCtx(gridCtx);
        }
    }

    function drawGridLines(gl, program) {
        gl.useProgram(program);

        let numOfPoints = gridObj.bufferData.length / 2;
        let lineColorHex = myState.config.getColorMode(myState.fractalConfig);
        let lineColorGl = frontendUtil.getGLColorFromHex(lineColorHex, 1.0);

        gl.uniform2f(gl.getUniformLocation(program, "u_resolution"), gl.canvas.width, gl.canvas.height);
        gl.uniform1f(gl.getUniformLocation(program, "point_size"), myState.config.gridPointSize);

        gl.uniform4fv(gl.getUniformLocation(program, "color"), lineColorGl);
        gl.bindVertexArray(gridObj.vao);
        gl.drawArrays(gl.LINES, 0, numOfPoints);
    }

    /**
     * The render function which draws the scene.
     * @param gl the WebG: context to draw the scene to
     */
    function renderDivergenceLine(gl, program) {
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

        // Clear canvas
        gl.clear(gl.COLOR_BUFFER_BIT);

        if (!myState.config.divergenceLine.hidden) {
            // Hide line
            drawDivergenceLine(gl, program);
        }
    }

    function drawDivergenceLine(gl, program) {
        gl.useProgram(program);

        let numOfPoints = divergenceLineObj.bufferData.length / 2;

        let pointColorHex = myState.config.getColorMode(myState.fractalConfig);
        let lineColorHex = myState.config.divergenceLine.lineColorHex;
        let pointColorGl = frontendUtil.getGLColorFromHex(pointColorHex, 1.0);
        let lineColorGl = frontendUtil.getGLColorFromHex(lineColorHex, 1.0);

        // Drive line strip
        if (myState.config.divergenceLine.pointsOnly === false) {
            gl.uniform1f(gl.getUniformLocation(program, "point_size"), 1.0);
            gl.uniform4fv(gl.getUniformLocation(program, "color"), lineColorGl);
            gl.uniform1i(gl.getUniformLocation(program, "point_shading"), false);
            gl.bindVertexArray(divergenceLineObj.vao);
            gl.drawArrays(gl.LINE_STRIP, 0, numOfPoints);
            gl.bindVertexArray(null);
        }

        // Draw points
        gl.uniform1f(gl.getUniformLocation(program, "point_size"), myState.config.divergenceLine.pointSize);
        gl.uniform4fv(gl.getUniformLocation(program, "color"), pointColorGl);
        gl.uniform1i(gl.getUniformLocation(program, "point_shading"), true);
        gl.bindVertexArray(divergenceLineObj.vao);
        gl.drawArrays(gl.POINTS, 0, numOfPoints);
        gl.bindVertexArray(null);
    }

    function drawMandelbrot(gl, program) {
        // Use program
        gl.useProgram(program);

        // Set uniforms
        gl.uniform1i(gl.getUniformLocation(program, "color_sheme"), myState.fractalConfig.colorSheme);
        gl.uniform1f(gl.getUniformLocation(program, "xMin"), myState.fractalConfig.xMin);
        gl.uniform1f(gl.getUniformLocation(program, "xMax"), myState.fractalConfig.xMax);
        gl.uniform1f(gl.getUniformLocation(program, "yMin"), myState.fractalConfig.yMin);
        gl.uniform1f(gl.getUniformLocation(program, "yMax"), myState.fractalConfig.xMax);

        gl.uniform1f(gl.getUniformLocation(program, "uMin"), myState.fractalConfig.uMin);
        gl.uniform1f(gl.getUniformLocation(program, "uMax"), myState.fractalConfig.uMax);
        gl.uniform1f(gl.getUniformLocation(program, "vMin"), myState.fractalConfig.vMin);
        gl.uniform1f(gl.getUniformLocation(program, "vMax"), myState.fractalConfig.vMax);
        gl.uniform1i(gl.getUniformLocation(program, "limit"), myState.fractalConfig.limits.currDepthLimit);
        gl.uniform1f(gl.getUniformLocation(program, "exponent"), myState.fractalConfig.exponent);

        gl.bindVertexArray(mandelbrotObj.vao);
        let numOfPoints = mandelbrotObj.bufferData.length / 2;
        gl.drawArrays(gl.TRIANGLES, 0, numOfPoints);
    }


    /**
     * Initializes common webGL settings like clear color.
     * @param gl the webGL context to initialize settings for
     */
    function initWebGLSettings(gl, color, alpha) {
        let c = frontendUtil.getGLColorFromHex(color, alpha);
        gl.clearColor(c[0], c[1], c[2], c[3]);
    }


    this.updateCoordinates = function (m) {
        let y = gridCtx.canvas.height - m.y;
        let complexPoint = getComplexPointFromRaster(myState.fractalConfig, m.x, m.y);
        if (myState.config.hasUiToolbox) {
            myState.complexX.innerHTML = complexPoint.x.toFixed(4);
            myState.complexY.innerHTML = complexPoint.y.toFixed(4);
            myState.rasterX.innerHTML = m.x.toFixed(4);
            myState.rasterY.innerHTML = m.y.toFixed(4);
        }
        myState.coords = {
            complexX: complexPoint.x,
            complexY: complexPoint.y,
            rasterX: m.x,
            rasterY: y
        };
        initGridLines(undefined, false);
    };


    function initWebGLVAOBuffer(gl, obj) {
        let location = obj.location;

        let positionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(obj.bufferData), gl.STATIC_DRAW);

        obj.vao = gl.createVertexArray();
        gl.bindVertexArray(obj.vao);
        gl.enableVertexAttribArray(location);
        gl.vertexAttribPointer(location, obj.numOfCoordinates, gl.FLOAT, obj.normalize || false, 0, 0);

        // Unbind the vao object
        gl.bindVertexArray(null);
    }


    let myState = this;
    // Set coords
    myState.coords = {
        complexX: 0.0,
        complexY: 0.0,
        rasterX: 0.0,
        rasterY: 0.0
    };

    myState.canvas = canvasMandelbrot;
    myState.canvasDivLine = canvasDivLine;

    updateCanvas(canvasMandelbrot);
    updateCanvas(canvasDivLine);
    updateCanvas(gridTextCanvas);

    // Init buffer data
    initTriangles(mandelbrotObj);
    initPointsDivLine(divergenceLineObj);
    initGridLines(gridObj);

    initWebGLVAOBuffer(glMandelbrot, mandelbrotObj);
    initWebGLVAOBuffer(glMandelbrot, gridObj);
    initWebGLVAOBuffer(glDivLine, divergenceLineObj);

    return myState;
}


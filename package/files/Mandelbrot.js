/**
 * Mandelbrot fractal.
 * @param width the width of the raster to create fractal in
 * @param height the height of the raster to create fractal in
 * @constructor creates mandelbrot fractal with the specified width and height
 */
function Mandelbrot(width, height) {
    let xMin = 0.0;
    let xMax = width;
    let yMin = 0.0;
    let yMax = height;

    // Complex coordinate system
    let uMin = -2.0;
    let uMax = 1.0;
    let vMin = -1.2;
    let vMax = 1.2;

    let depthLimit = 100;

    let vertexData = [];
    let colorData = [];

    let fu = new FrontendUtil();
    collectData();

    function divergenceTest(c, limit) {
        let z = new ComplexNumber();
        for (let i = 1; i <= limit; i++) {
            let nextX = z.x * z.x - z.y * z.y + c.x;
            let nextY = 2 * z.x * z.y + c.y;
            z.x = nextX;
            z.y = nextY;

            let modulSquared = z.modulSquared();
            if (modulSquared > 4) {
                return i;
            }
        }

        return -1;
    }


    function collectData() {
        for (let y = yMin; y <= yMax; y++) {
            for (let x = xMin; x <= xMax; x++) {
                let c = new ComplexNumber();
                c.x = (x - xMin) / (xMax - xMin) * (uMax - uMin) + uMin;
                c.y = (y - yMin) / (yMax - yMin) * (vMax - vMin) + vMin;

                let n = divergenceTest(c, depthLimit);

                if (n === -1) {
                    let color = fu.colors.BLACK;
                    colorData.push(color.r, color.g, color.b);
                } else {
                    let color = fu.colors.PURPLE;
                    colorData.push(color.r, color.g, color.b);
                }

                vertexData.push(x, y);
            }
        }
    }


    return {
        vertices: vertexData,
        colors: colorData
    };
}


function WebGL2JuliaRenderer(canvas, config) {
    let frontendUtil = new FrontendUtil();
    this.constants = getConstants();

    // Listeners setup
    this.animation = {
        then: 5,
        lastMouseX: 0,
        lastMouseY: 0,
        scrollConst: 1.2,
        rotationSpeed: 20,
        enabled: false
    };

    this.config = config;
    this.fractalConfig = config;

    // Init Canvas properties
    canvas.width = config.xMax || 300;
    canvas.height = config.yMax || 300;

    // Init webGL
    let gl = initWebGL2(canvas);
    initWebGLSettings(gl);

    // Init shaders
    let program = initShaders(gl, "julia-vertex-shader", "julia-fragment-shader");

    // Setup locations
    let a_position = getAttribLocation(gl, program, "position");
    let juliaObj = {
        location: a_position,
        numOfCoordinates: 2,
        normalize: false,
        bufferData: [],
        vao: undefined
    };


    function initTriangles() {
        juliaObj.bufferData = [].concat(
            triangulateRectangle(-1.0, -1.0, 2.0, 2.0),
        );
    }


    /**
     * Draws the scene.
     */
    this.invalidate = function () {
        updateCanvas();
        if (myState.recompileShaders) {
            recompileShaders();
            myState.recompileShaders = false;
        }
        drawScene(gl);
    };


    function updateCanvas() {
        myState.canvas.width = myState.fractalConfig.xMax;
        myState.canvas.height = myState.fractalConfig.yMax;
    }


    function recompileShaders() {
        let shaders = gl.getAttachedShaders(program);
        gl.detachShader(program, shaders[0]);
        gl.detachShader(program, shaders[1]);
        updateShaders(gl, program, "julia-vertex-shader", "julia-fragment-shader");
    }


    this.setInitialPoint = function (complexNumber) {
        myState.fractalConfig.c = complexNumber;
    };

    this.setColorSheme = function (colorSheme) {
        myState.fractalConfig.colorSheme = colorSheme;
    };


    /**
     * The render function which draws the scene.
     * @param gl the WebG: context to draw the scene to
     */
    function drawScene(gl) {
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

        // Clear canvas
        gl.clear(gl.COLOR_BUFFER_BIT);

        // Use program
        gl.useProgram(program);

        // Set uniforms
        gl.uniform1i(gl.getUniformLocation(program, "color_sheme"), myState.fractalConfig.colorSheme);
        gl.uniform1f(gl.getUniformLocation(program, "xMin"), myState.fractalConfig.xMin);
        gl.uniform1f(gl.getUniformLocation(program, "xMax"), myState.fractalConfig.xMax);
        gl.uniform1f(gl.getUniformLocation(program, "yMin"), myState.fractalConfig.yMin);
        gl.uniform1f(gl.getUniformLocation(program, "yMax"), myState.fractalConfig.xMax);

        gl.uniform1f(gl.getUniformLocation(program, "uMin"), myState.fractalConfig.uMin);
        gl.uniform1f(gl.getUniformLocation(program, "uMax"), myState.fractalConfig.uMax);
        gl.uniform1f(gl.getUniformLocation(program, "vMin"), myState.fractalConfig.vMin);
        gl.uniform1f(gl.getUniformLocation(program, "vMax"), myState.fractalConfig.vMax);
        gl.uniform2f(gl.getUniformLocation(program, "initial_complex_number"), myState.fractalConfig.c.x, myState.fractalConfig.c.y);
        gl.uniform1f(gl.getUniformLocation(program, "exponent"), myState.fractalConfig.exponent);
        gl.uniform1i(gl.getUniformLocation(program, "limit"), myState.fractalConfig.depthLimit);

        gl.bindVertexArray(juliaObj.vao);
        let numOfPoints = juliaObj.bufferData.length / 2;
        gl.drawArrays(gl.TRIANGLES, 0, numOfPoints);
    }


    /**
     * Initializes common webGL settings like clear color.
     * @param gl the webGL context to initialize settings for
     */
    function initWebGLSettings(gl) {
        let c = frontendUtil.getGLColorFromHex("#fef9ff", 1.0);
        gl.clearColor(c[0], c[1], c[2], c[3]);
    }


    this.signalRendering = function (rendering) {
        myState.canvas.className = (rendering) ? "canvas-active" : "canvas-passive";
    };

    this.updateLater = function (afterFinish) {
        myState.signalRendering(true);
        window.setTimeout(function () {
            myState.invalidate();
            if (afterFinish) {
                afterFinish();
            }
            myState.signalRendering(false);
        }, 100);
    };


    this.updateCoordinates = function (m) {
        let complexPoint = getComplexPointFromRaster(myState.fractalConfig, m.x, m.y);
        myState.complexX.innerHTML = complexPoint.x.toFixed(4);
        myState.complexY.innerHTML = complexPoint.y.toFixed(4);
        myState.rasterX.innerHTML = m.x.toFixed(4);
        myState.rasterY.innerHTML = m.y.toFixed(4);
    };




    function initWebGLVAOBuffer(gl, obj) {
        let location = obj.location;

        let positionBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(obj.bufferData), gl.STATIC_DRAW);

        obj.vao = gl.createVertexArray();
        gl.bindVertexArray(obj.vao);
        gl.enableVertexAttribArray(location);
        gl.vertexAttribPointer(location, obj.numOfCoordinates, gl.FLOAT, obj.normalize || false, 0, 0);

        // Unbind the vao object
        gl.bindVertexArray(null);
    }


    let myState = this;
    myState.canvas = canvas;

    initTriangles(juliaObj);
    updateCanvas();

    initWebGLVAOBuffer(gl, juliaObj);

    return myState;
}
/**
 * Helper function which adds all needed event listeners.
 */
function addEventListenersJulia(state, sliders) {
    // function keyDownCallback(e) {
    //     eventKeyDown3D(e, state);
    //     state.invalidate();
    // }


    function containedIn(x, y, w, h, m) {
        let mx = m.x;
        let my = m.y;
        return (mx > x) && (mx < (x + w)) &&
            (my > y) && (my <= (y + h));
    }


    function windowMouseDown(e) {
        let m = getMouse(state.canvas, e);
        let c = state.canvas;
        let r = c.getBoundingClientRect();

        state.animation.enabled = containedIn(0, 0, r.width, r.height, m);
        return true;
    }


    function mouseDownCallback(e) {
        eventMouseDown3D(e, state);
        if (state.animation.enabled) {
            let x = state.animation.lastMouseX;
            let y = state.animation.lastMouseY;

            zoomWith(getComplexPointFromRaster(state.fractalConfig, x, state.canvas.height - y), state.fractalConfig.defaultZoomScale, state);
        }

        state.updateLater();
    }


    function wheelMoveCallback(e) {
        eventMouseDown3D(e, state);
        let x = state.animation.lastMouseX;
        let y = state.animation.lastMouseY;

        let defaultScale = state.fractalConfig.defaultZoomScale;
        let scale = Math.sign(e.deltaY) === -1 ? defaultScale : 1 / defaultScale;
        let complexPoint = getComplexPointFromRaster(state.fractalConfig, x, state.canvas.height - y);
        if (state.fractalConfig.zoomLocked) {
            // Complex point is 0.0
            complexPoint = {
                x: 0.0,
                y: 0.0
            };
        }

        zoomWith(complexPoint, scale, state);

        // Update sliders actual values
        if (sliders) {
            sliders.cmplxPlaneMaxX.updateValue(state.fractalConfig.uMax);
            sliders.cmplxPlaneMinX.updateValue(state.fractalConfig.uMin);
            sliders.cmplxPlaneMaxY.updateValue(state.fractalConfig.vMax);
            sliders.cmplxPlaneMinY.updateValue(state.fractalConfig.vMin);
        }

        e.preventDefault();
        state.updateLater();
    }


    function mouseMoveCallback(e) {
        let m = getMouse(state.canvas, e);
        m.y = state.canvas.height - m.y;
        state.updateCoordinates(m);
    }


    let can = state.canvas;
    can.addEventListener("mousedown", mouseDownCallback, false);
    can.addEventListener("mousemove", mouseMoveCallback, false);
    window.addEventListener("mousedown", windowMouseDown, true);
    addWheelListener(can, wheelMoveCallback);
}


function initConfigurationToolboxJulia(state, suffix) {
    let defaultZoomScaleOpts = {
        min: 2,
        max: 16,
        step: 1,
        value: state.fractalConfig.defaultZoomScale,
        functionCallback: updateFractalConfig("defaultZoomScale")
    };

    let rasterWidthOpts = {
        min: 100,
        max: 600,
        step: 1,
        value: state.fractalConfig.xMax,
        functionCallback: updateFractalConfig("xMax")
    };

    let rasterHeightOpts = {
        min: 100,
        max: 600,
        step: 1,
        value: state.fractalConfig.yMax,
        functionCallback: updateFractalConfig("yMax")
    };

    let complexUMaxSliderOpts = {
        uiPrecision: 1,
        min: -5,
        max: 5,
        step: 0.1,
        value: state.fractalConfig.uMax,
        functionCallback: updateFractalConfig("uMax")
    };

    let complexUMinSliderOpts = {
        uiPrecision: 1,
        min: -5,
        max: 5,
        step: 0.1,
        value: state.fractalConfig.uMin,
        functionCallback: updateFractalConfig("uMin")
    };


    let complexVMaxSliderOpts = {
        uiPrecision: 1,
        min: -5,
        max: 5,
        step: 0.1,
        value: state.fractalConfig.vMax,
        functionCallback: updateFractalConfig("vMax")
    };

    let complexVMinSliderOpts = {
        uiPrecision: 1,
        min: -5,
        max: 5,
        step: 0.1,
        value: state.fractalConfig.vMin,
        functionCallback: updateFractalConfig("vMin")
    };

    let depthLimitOpts = {
        min: 10,
        max: 10 * 1000,
        step: 1,
        value: state.fractalConfig.depthLimit,
        functionCallback: updateDepthLimit(state)
    };

    let zExponentOpts = {
        uiPrecision: 1,
        min: 0.5,
        max: 10.0,
        step: 0.1,
        value: state.fractalConfig.exponent,
        functionCallback: updateFractalConfig("exponent")
    };


    function updateDepthLimit(state) {
        return function (event, ui) {
            state.fractalConfig.depthLimit = ui.value;
        }
    }


    function updateFractalConfig(configVariable) {
        return function (event, ui) {
            state.fractalConfig[configVariable] = ui.value;
            state.updateLater();
        }
    }

    let suffixID = suffix || "";
    let rasterWidthSliderId = "rasterWidth" + suffixID;
    let rasterHeightSliderId = "rasterHeight" + suffixID;
    let complexUMaxSliderId = "uMax" + suffixID;
    let complexUMinSliderId = "uMin" + suffixID;
    let complexVMaxSliderId = "vMax" + suffixID;
    let complexVMinSliderId = "vMin" + suffixID;
    let depthLimitId = "depthLimit" + suffixID;
    let zoomScaleId = "zoomScale" + suffixID;
    let zExponent = "zExponent" + suffixID;
    setupSlider(rasterWidthSliderId, rasterWidthOpts);
    setupSlider(rasterHeightSliderId, rasterHeightOpts);
    let cmplxPlaneMaxX = setupSlider(complexUMaxSliderId, complexUMaxSliderOpts);
    let cmplxPlaneMinX = setupSlider(complexUMinSliderId, complexUMinSliderOpts);
    let cmplxPlaneMaxY = setupSlider(complexVMaxSliderId, complexVMaxSliderOpts);
    let cmplxPlaneMinY = setupSlider(complexVMinSliderId, complexVMinSliderOpts);
    let depthLimitSlider = setupSlider(depthLimitId, depthLimitOpts);
    setupSlider(zExponent, zExponentOpts);
    setupSlider(zoomScaleId, defaultZoomScaleOpts);


    // Render button event callback
    let renderButton = document.getElementById("render-button" + suffixID);
    renderButton.onclick = e => renderClickCallback(e, state);

    return {
        depthLimitSlider: depthLimitSlider,
        cmplxPlaneMaxX: cmplxPlaneMaxX,
        cmplxPlaneMinX: cmplxPlaneMinX,
        cmplxPlaneMaxY: cmplxPlaneMaxY,
        cmplxPlaneMinY: cmplxPlaneMinY,
    }
}

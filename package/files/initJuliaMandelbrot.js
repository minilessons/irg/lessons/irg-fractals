(function init() {
    let juliaRenderer;


    function initMandelbrot() {
        let suffixID = "-m";
        let canvasM = document.getElementById("mandelbrot_canvas");

        let canvasWidth = 600;
        let canvasHeight = 600;
        let config = {
            // Translations
            convergesText: "Da",
            divergesText: "Ne",

            // Toolbox
            hasUiToolbox: true,
            showCoords: false,

            gridStep: 30, // Raster; Pixels
            gridPointSize: 1.0,
            gridTickSize: 15, // Raster; Pixels
            gridTickTextOffset: 10, // Raster; Pixels
            showGrid: false,
            // gridColorHex: "#717f85",
            // gridColorHex: "#88e663",

            getColorMode: function (cnf) {
                if (cnf.colorSheme !== COLOR_SHEMES.GRAY) {
                    return "#FFFFFF" // white
                } else {
                    return "#002233" // black
                }
            },
            
            updateAll: false,
            mandelbrotNeedUpdate: true,
            divLineNeedUpdate: true,
            userConfig: {
                defaultZoomScale: 1.5,
                // Raster coordinate system
                xMin: 0.0,
                xMax: canvasWidth,
                yMin: 0.0,
                yMax: canvasHeight,

                // Complex coordinate system
                zoomLocked: true,
                uMin: -2.3,
                uMax: 2.3,
                vMin: -2.3,
                vMax: 2.3,
                limits: {
                    min: 1,
                    max: 20 * 1000, // Was 100 * 1000; 100k
                    stepMult: 2,
                    step: 2,
                    default: 1000,
                    currDepthLimit: 1000
                },
                hidden: false,
                colorSheme: COLOR_SHEMES.SIMPLE_RED,
                exponent: 2
            },

            divergenceLine: {
                pointColorHex: "#000000",
                lineColorHex: "#FF0000",
                pointSize: 4.0,
                hidden: true,
                initialPoint: new ComplexNumber(), // (0.0, 0.0)
                lastMovePoint: new ComplexNumber(1, 1),
                converges: false,

                convIterLimit: 256,
                stopThreshold: 200,

                pointsOnly: false
            },
            zoomOutComplexXMax: 25,
            zoomOutComplexYMax: 20,
            zoomWithClickEnabled: false,
        };
        let mandelbrotRenderer = new WebGL2MandelbrotRenderer(canvasM, config);

        // Init configuration toolbox
        let sliders = initConfigurationToolboxMandelbrot(mandelbrotRenderer, suffixID);
        initZoomLockSetting(mandelbrotRenderer, "zoomLock" + suffixID);
        initSelectValuePicker(mandelbrotRenderer, COLOR_SHEMES, "colorSheme" + suffixID, "colorSheme");

        // Init coordinates
        initCoordinatesWithSuffix(mandelbrotRenderer, suffixID);

        // Add listeners to div line canvas, but actual logic is performed on Mandelbrot canvas
        addEventListenersMandelbrot(mandelbrotRenderer, mandelbrotRenderer.canvasDivLine, sliders);

        // Add key down listeners
        // For depth limit divergence changes
        mandelbrotRenderer.canvasDivLine.addEventListener("keyup", (e) => keyUpCallback(e, mandelbrotRenderer, sliders), false);

        // Add another mouse down listener but for right mouse click which will update julia canvas
        // canvasM.addEventListener("mousedown", mouseDownJuliaCallback, false);
        mandelbrotRenderer.canvasDivLine.addEventListener("mousedown", (e) => mouseDownCallback(e, mandelbrotRenderer, mandelbrotRenderer.canvasDivLine), false);
        // canvasM.addEventListener("mousemove", mouseMoveCallback, false);
        mandelbrotRenderer.canvasDivLine.addEventListener("mousemove", (e) => mouseMoveCallback(e, mandelbrotRenderer, mandelbrotRenderer.canvasDivLine), false);
        // canvasM.addEventListener("mouseup", mouseUpCallback, false);
        mandelbrotRenderer.canvasDivLine.addEventListener("mouseup", (e) => mouseUpCallback(e, mandelbrotRenderer), false);
        // Disable on canvas
        disableContextMenu(mandelbrotRenderer.canvasDivLine);
        mandelbrotRenderer.updateLater();

        function mouseMoveCallback(e, state, canvas) {
            // Mandelbrot update for divergence line
            if (mandelbrotRenderer.animation.divLineDragging) {
                updateDivLine(e, state, canvas);
                mandelbrotRenderer.config.divLineNeedUpdate = true;
                mandelbrotRenderer.invalidate();
            }

            if (mandelbrotRenderer.animation.dragging) {
                // Julia update
                updateJulia(e);
            }
        }

        function updateDivLine(e, state, canvas) {
            let mouse = getMouse(canvas, e);
            let x = mouse.x;
            let y = canvas.height - mouse.y;
            // To complex converted later, once divergence test is called
            // let complexPoint = getComplexPointFromRaster(state.fractalConfig, x, y);
            state.config.divergenceLine.lastMovePoint = { x: x, y: y }; // new ComplexNumber(complexPoint.x, complexPoint.y);
        }


        function updateJulia(e) {
            let mouse = getMouse(canvasM, e);
            let x = mouse.x;
            let y = mandelbrotRenderer.canvas.height - mouse.y;
            let complexPoint = getComplexPointFromRaster(mandelbrotRenderer.fractalConfig, x, y);

            juliaRenderer.setInitialPoint(new ComplexNumber(complexPoint.x, complexPoint.y));
            juliaRenderer.invalidate();
        }


        function mouseUpCallback(e, state) {
            state.animation.dragging = false;
            state.animation.divLineDragging = false;
        }

        function mouseDownCallback(e, state, canvas) {
            // Mandelbrot setup
            if (isLeftClick(e)) {
                state.animation.divLineDragging = true;
                state.config.divergenceLine.hidden = false;
                updateDivLine(e, state, canvas);
                mandelbrotRenderer.config.divLineNeedUpdate = true;
                mandelbrotRenderer.invalidate();
            }

            // Julia Animation Setup
            if (isRightClick(e)) {
                state.animation.dragging = true;
                updateJulia(e);
            }
        }
    }


    function initJulia() {
        let suffixID = "-j";
        let canvasJ = document.getElementById("julia_canvas");

        let canvasWidth = 600;
        let canvasHeight = 600;
        let juliaConfig = {
            zoomLocked: true,

            defaultZoomScale: 2,
            // Raster coordinate system
            xMin: 0.0,
            xMax: canvasWidth,
            yMin: 0.0,
            yMax: canvasHeight,
    
            // Complex coordinate system
            uMin: -1.5,
            uMax: 1.0,
            vMin: -1.0,
            vMax: 1.0,
            depthLimit: 2 * 1000,
            exponent: 2.0,
            c: new ComplexNumber(-0.67, 0.34),
            colorSheme: COLOR_SHEMES.SIMPLE_RED
        };
    
        juliaRenderer = new WebGL2JuliaRenderer(canvasJ, juliaConfig);
        juliaRenderer.setInitialPoint(new ComplexNumber(-0.807984, -0.145732));
        juliaRenderer.setColorSheme(COLOR_SHEMES.SIMPLE_RED);

        // Init configuration toolbox
        sliders = initConfigurationToolboxJulia(juliaRenderer, suffixID);
        initSelectValuePicker(juliaRenderer, COLOR_SHEMES, "colorSheme" + suffixID, "colorSheme");
        initZoomLockSetting(juliaRenderer, "zoomLock" + suffixID);

        // Init coordinates
        initCoordinatesWithSuffix(juliaRenderer, suffixID);

        // Add listeners
        addEventListenersJulia(juliaRenderer, sliders);
        disableContextMenu(juliaRenderer);

        juliaRenderer.updateLater()
    }


    // ======================= Mandelbrot Fractal =======================
    initMandelbrot();
    // ======================= JULIA Fractal =======================
    initJulia();
})();
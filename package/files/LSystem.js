let fu = new FrontendUtil();

// *****************************
// Context & State
// *****************************
function TurtleContext() {
    let statesStack = [];
    this.vertexData = [];
    this.colorData = [];
    this.push = function (state) {
        statesStack.unshift(state);
    };

    this.pop = function () {
        statesStack.shift();
    };

    this.getCurrentState = function () {
        return statesStack[0];
    };

    this.enableLineTriangulization = false;

    this.addToPath = function (sx, sy, ex, ey, color, pointSize) {
        if (this.enableLineTriangulization) {
            let triangulated = triangulateLine(sx, sy, ex, ey, pointSize);
            for (let c = 0, l = triangulated.length; c < l; c++) {
                this.vertexData.push(triangulated[c])
            }
        } else {
            this.vertexData.push(sx, sy, ex, ey);
        }

        let glC = fu.getGLColorFromHex(color, 1.0);
        let numOfColors = this.enableLineTriangulization ? 6 : 2;
        for (let c = 0; c < numOfColors; c++) {
            this.colorData.push(glC[0], glC[1], glC[2], glC[3]);
        }
    };
}


function setInitialValues(state, jsonData) {
    let unitLength = jsonData.unitLength === undefined ? 0.1 : jsonData.unitLength;
    let level = jsonData.level === undefined ? 5 : jsonData.level;
    let unitLengthDegreeScaler = jsonData.unitLengthDegreeScaler === undefined ? 1.0 : jsonData.unitLengthDegreeScaler;
    state.angle = jsonData.angle === undefined ? 0 : jsonData.angle;
    state.position = jsonData.origin ? jsonData.origin : new Vector2D(0, 0);
    state.direction = jsonData.direction ? jsonData.direction :
        new Vector2D(Math.cos(fu.degToRad(state.angle)), Math.sin(fu.degToRad(state.angle)));
    //state.drawColor = jsonData.color ? jsonData.color : "#FF00FF";
    state.moveUnit = jsonData.moveUnit === undefined ?
        unitLength * Math.pow(unitLengthDegreeScaler, level) : jsonData.moveUnit;
    state.lineThickness = jsonData.lineThickness === undefined ? 0.001 : jsonData.lineThickness;
}


function TurtleState() {
    this.position = new Vector2D(0, 0);
    this.direction = new Vector2D(1, 0);
    this.moveUnit = 1.0;
    this.drawColor = "#000000";
    this.angle = 0;
    this.lineThickness = 0.001;

    this.copy = function () {
        let stateCopy = new TurtleState();
        stateCopy.angle = this.angle;
        stateCopy.position = this.position.copy();
        stateCopy.direction = this.direction.copy();
        stateCopy.drawColor = this.drawColor;
        stateCopy.moveUnit = this.moveUnit;
        stateCopy.lineThickness = this.lineThickness;

        return stateCopy;
    }
}


// *****************************
// Command Utils
// *****************************
function executeDraw(ctx, step, paint) {
    let currentState = ctx.getCurrentState();
    let currentColor = currentState.drawColor;

    let effectiveLength = currentState.moveUnit;
    let moveLength = effectiveLength * step;

    let oldPosition = currentState.position;
    let newPosition = oldPosition.translated(currentState.direction.scaled(moveLength));

    if (paint) {
        ctx.addToPath(
            oldPosition.x,
            oldPosition.y,
            newPosition.x,
            newPosition.y,
            currentColor,
            currentState.lineThickness);
    }
    // Save new position
    currentState.position = newPosition;
}


// *****************************
// Commnads
// *****************************
class Command {
    execute(ctx) {
        throw new Error("Not implemented!");
    }
}


class RotateCommand extends Command {
    constructor(angle) {
        super();
        this.angle = angle;
    }


    execute(ctx) {
        let state = ctx.getCurrentState();
        state.direction.rotate(this.angle);
    }
}


class SkipCommand extends Command {
    constructor(step) {
        super();
        this.step = step;
    }


    execute(ctx) {
        executeDraw(ctx, this.step, false);
    }
}


class DrawCommand extends Command {
    constructor(step) {
        super();
        this.step = step;
    }


    execute(ctx) {
        executeDraw(ctx, this.step, true);
    }
}


class PopCommand extends Command {
    constructor() {
        super();
    }


    execute(ctx) {
        ctx.pop();
    }
}


class PushCommand extends Command {
    constructor() {
        super();
    }


    execute(ctx) {
        ctx.push(ctx.getCurrentState().copy());
    }
}


class PointSizeCommand extends Command {
    constructor(factor) {
        super();
        this.factor = factor;
    }


    execute(ctx) {
        let state = ctx.getCurrentState();
        state.lineThickness -= this.factor;
        if (state.lineThickness <= 0.001) {
            state.lineThickness = 0.001;
        }
    }
}


class RandomColorCommand extends Command {
    constructor() {
        super();
    }


    execute(ctx) {
        ctx.getCurrentState().drawColor = "#" + generateHexDigits(6);
    }
}


class ColorCommand extends Command {
    constructor(color) {
        super();
        this.color = color;
    }


    execute(ctx) {
        ctx.getCurrentState().drawColor = this.color;
    }
}


class ScaleCommand extends Command {
    constructor(factor) {
        super();
        this.factor = factor;
    }


    execute(ctx) {
        let currentState = ctx.getCurrentState();
        currentState.moveUnit = currentState.moveUnit * this.factor;
    }
}
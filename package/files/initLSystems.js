(function init() {
    // Init test fractal
    let canvas = document.getElementById("l_systems_canvas");
    let renderer = new LSystemRenderer(canvas);

    renderer.rasterX = document.getElementById("raster-x");
    renderer.rasterY = document.getElementById("raster-y");

    initSelectValuePicker(renderer, L_SYSTEM_TYPE, "l_system_type", "lSystemType");
    initConfigurationToolboxMandelbrot(renderer);

    renderer.updateLater();
})();


function initConfigurationToolboxMandelbrot(state, suffix) {
    let rasterWidthOpts = {
        min: 100,
        max: 800,
        step: 1,
        value: state.fractalConfig.xMax,
        functionCallback: updateFractalConfig("xMax")
    };

    let rasterHeightOpts = {
        min: 100,
        max: 800,
        step: 1,
        value: state.fractalConfig.yMax,
        functionCallback: updateFractalConfig("yMax")
    };

    let depthLimitOpts = {
        min: 1,
        max: 20,
        step: 1,
        value: state.fractalConfig.depthLimit,
        functionCallback: updateDepthLimit(state)
    };


    function updateDepthLimit(state) {
        return function (event, ui) {
            state.fractalConfig.depthLimit = ui.value;
        }
    }


    function updateFractalConfig(configVariable) {
        return function (event, ui) {
            state.fractalConfig[configVariable] = ui.value;
        }
    }


    let suffixID = suffix || "";
    let rasterWidthSliderId = "rasterWidth" + suffixID;
    let rasterHeightSliderId = "rasterHeight" + suffixID;
    let depthLimitId = "level" + suffixID;
    setupSlider(rasterWidthSliderId, rasterWidthOpts);
    setupSlider(rasterHeightSliderId, rasterHeightOpts);
    setupSlider(depthLimitId, depthLimitOpts);


    // Render button event callback
    let renderButton = document.getElementById("render-button" + suffixID);
    renderButton.onclick = e => renderClickCallback(e, state);
}


function executeAction(model, symbol, ctx) {
    switch (symbol) {
        case "F":
            executeDraw(ctx, 1, true);
            break;
        case "+":
            ctx.getCurrentState().direction.rotate(25);
            break;
        case "-":
            ctx.getCurrentState().direction.rotate(-25);
            break;
        case "[":
            ctx.push(ctx.getCurrentState().copy());
            break;
        case "]":
            ctx.pop();
            break;
    }
}


function LSystem(data) {
    this.generateIteratively = function (level) {
        let s = data.axiom;
        for (let c = 0; c < level; c++) {
            let newStr = "";
            for (let idx = 0; idx < s.length; idx++) {
                let newData = data.productions[s[idx]];
                if (newData) {
                    newStr += newData
                } else {
                    newStr += s[idx]
                }
            }
            s = newStr
        }
        return s;
    };


    this.generateState = function (level) {
        if (level === 0) {
            return data.axiom;
        }
        return applyProduction(this.generateState(level - 1));
    };


    function applyProduction(rightSide) {
        let sb = "";
        for (let c = 0; c < rightSide.length; c++) {
            let symbol = rightSide[c];
            if (data.productions[symbol]) {
                sb += data.productions[symbol]
            } else {
                sb += symbol;
            }
        }
        return sb;
    }
}
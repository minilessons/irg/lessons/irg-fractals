function divergenceTestWithLog(z0, c, limit, stopTrehshold = 4) {
    let z = new ComplexNumber(z0.x, z0.y);
    let values = [];
    values.push(new ComplexNumber(z.x, z.y));

    for (let i = 1; i <= limit; i++) {
        let nextX = z.x * z.x - z.y * z.y + c.x;
        let nextY = 2 * z.x * z.y + c.y;
        z.x = nextX;
        z.y = nextY;

        // Log next value
        values.push(new ComplexNumber(nextX, nextY));

        let modulSquared = z.modulSquared();
        if (modulSquared > stopTrehshold) {
            let res = {
                values: values,
                status: i
            }
            return res;
        }
    }

    let res = {
        values: values,
        status: -1
    }
    return res;
}

function initConfigurationToolboxMandelbrot(state, suffix) {
    let defaultZoomScaleOpts = {
        min: 2,
        max: 16,
        step: 1,
        value: state.fractalConfig.defaultZoomScale,
        functionCallback: updateFractalConfig("defaultZoomScale")
    };

    let rasterWidthOpts = {
        min: 100,
        max: 600,
        step: 1,
        value: state.fractalConfig.xMax,
        functionCallback: updateFractalConfig("xMax")
    };

    let rasterHeightOpts = {
        min: 100,
        max: 600,
        step: 1,
        value: state.fractalConfig.yMax,
        functionCallback: updateFractalConfig("yMax")
    };

    let complexUMaxSliderOpts = {
        uiPrecision: 1,
        min: -5,
        max: 5,
        step: 0.1,
        value: state.fractalConfig.uMax,
        functionCallback: updateFractalConfig("uMax")
    };

    let complexUMinSliderOpts = {
        uiPrecision: 1,
        min: -5,
        max: 5,
        step: 0.1,
        value: state.fractalConfig.uMin,
        functionCallback: updateFractalConfig("uMin")
    };


    let complexVMaxSliderOpts = {
        uiPrecision: 1,
        min: -5,
        max: 5,
        step: 0.1,
        value: state.fractalConfig.vMax,
        functionCallback: updateFractalConfig("vMax")
    };

    let complexVMinSliderOpts = {
        uiPrecision: 1,
        min: -5,
        max: 5,
        step: 0.1,
        value: state.fractalConfig.vMin,
        functionCallback: updateFractalConfig("vMin")
    };

    let depthLimitOpts = {
        min: state.fractalConfig.limits.min,
        max: state.fractalConfig.limits.max,
        step: state.fractalConfig.limits.step,
        value: state.fractalConfig.limits.default,
        functionCallback: updateDepthLimit(state)
    };

    let zExponentOpts = {
        uiPrecision: 1,
        min: 0.5,
        max: 10.0,
        step: 0.1,
        value: state.fractalConfig.exponent,
        functionCallback: updateFractalConfig("exponent")
    };

    function updateDepthLimit(state) {
        return function (event, ui) {
            state.fractalConfig.limits.currDepthLimit = ui.value;
        }
    }


    function updateFractalConfig(configVariable) {
        return function (event, ui) {
            state.fractalConfig[configVariable] = ui.value;
        }
    }


    let suffixID = suffix || "";
    let rasterWidthSliderId = "rasterWidth" + suffixID;
    let rasterHeightSliderId = "rasterHeight" + suffixID;
    let complexUMaxSliderId = "uMax" + suffixID;
    let complexUMinSliderId = "uMin" + suffixID;
    let complexVMaxSliderId = "vMax" + suffixID;
    let complexVMinSliderId = "vMin" + suffixID;
    let depthLimitId = "depthLimit" + suffixID;
    let zoomScaleId = "zoomScale" + suffixID;
    let zExponent = "zExponent" + suffixID;
    setupSlider(rasterWidthSliderId, rasterWidthOpts);
    setupSlider(rasterHeightSliderId, rasterHeightOpts);
    let cmplxPlaneMaxX = setupSlider(complexUMaxSliderId, complexUMaxSliderOpts);
    let cmplxPlaneMinX = setupSlider(complexUMinSliderId, complexUMinSliderOpts);
    let cmplxPlaneMaxY = setupSlider(complexVMaxSliderId, complexVMaxSliderOpts);
    let cmplxPlaneMinY = setupSlider(complexVMinSliderId, complexVMinSliderOpts);
    setupSlider(zExponent, zExponentOpts);
    let depthLimitSlider = setupSlider(depthLimitId, depthLimitOpts);
    setupSlider(zoomScaleId, defaultZoomScaleOpts);


    // Render button event callback
    let renderButton = document.getElementById("render-button" + suffixID);
    renderButton.onclick = e => renderClickCallback(e, state);

    return {
        depthLimitSlider: depthLimitSlider,
        cmplxPlaneMaxX: cmplxPlaneMaxX,
        cmplxPlaneMinX: cmplxPlaneMinX,
        cmplxPlaneMaxY: cmplxPlaneMaxY,
        cmplxPlaneMinY: cmplxPlaneMinY,
    }
}

function toFixed(num, decimalCount = 2) {
    return num.toFixed(decimalCount);
}

function roundNDecimals(num, decimalCount = 2) {
    return Math.round((num + Number.EPSILON) * decimalCount * 10) / decimalCount * 10;
}

function roundNDecimalsStr(num, decimalsCount = 2) {
    return parseFloat(num).toFixed(decimalsCount);
}


// Key Up callback
function keyUpCallback(e, state, sliders = undefined) {
    let x = e.which || event.keyCode;
    let code = state.constants.keyCodes;

    let successful = true;
    switch (x) {
        case code.F2:
            state.config.divergenceLine.pointsOnly = !state.config.divergenceLine.pointsOnly;
            state.config.divLineNeedUpdate = true;
            break;
        case code.H:
            // Hide coords, grid etc
            state.config.showCoords = !state.config.showCoords;
            break;
        case code.G:
            state.config.showGrid = !state.config.showGrid;
            break;
        case code.R:
            // Clear/Reset divergence line
            state.config.divergenceLine.hidden = true;
            state.config.divLineNeedUpdate = true;
            break;
        case code.I:
        case code.numpadPlus:
            let newLimit = state.fractalConfig.limits.currDepthLimit * state.fractalConfig.limits.stepMult;
            if (newLimit <= state.fractalConfig.limits.max) {
                state.fractalConfig.limits.currDepthLimit = newLimit;
            } else {
                alert("Maximum depth limit reached!");
            }
            if (sliders) {
                sliders.depthLimitSlider.updateValue(state.fractalConfig.limits.currDepthLimit);
            }
            break;
        case code.D:
        case code.numpadMinus:
            state.fractalConfig.limits.currDepthLimit /= state.fractalConfig.limits.stepMult;
            if (state.fractalConfig.limits.currDepthLimit < 1) {
                alert("Minimum depth limit reached!");
                state.fractalConfig.limits.currDepthLimit = 1;
            }

            if (sliders) {
                sliders.depthLimitSlider.updateValue(state.fractalConfig.limits.currDepthLimit);
            }
            break;
        case code.F1:
            // Show/Hide fractal
            state.fractalConfig.hidden = !state.fractalConfig.hidden;
            break;
        case code.F2:
            // Which action?
            break;
        default:
            successful = false;
    }

    if (successful) {
        // Mandelbrot needs update
        state.config.mandelbrotNeedUpdate = true;
        state.updateLater();
        e.preventDefault();
        return false;
    }
}


/**
 * Helper function which adds all needed event listeners.
 */
function addEventListenersMandelbrot(state, canDivLine, sliders) {
    // function keyDownCallback(e) {
    //     eventKeyDown3D(e, state);
    //     state.invalidate();
    // }
    let canvas = state.canvas;


    function containedIn(x, y, w, h, m) {
        let mx = m.x;
        let my = m.y;
        return (mx > x) && (mx < (x + w)) &&
            (my > y) && (my <= (y + h));
    }


    function windowMouseDown(e) {
        let m = getMouse(canvas, e);
        let c = canvas;
        let r = c.getBoundingClientRect();

        state.animation.enabled = containedIn(0, 0, r.width, r.height, m);
        return true;
    }


    function mouseDownCallback(e) {
        if (isRightClick(e)) {
            return;
        }

        eventMouseDown3D(e, state);
        if (state.animation.enabled) {
            let x = state.animation.lastMouseX;
            let y = state.animation.lastMouseY;
            let complexPoint = getComplexPointFromRaster(state.fractalConfig, x, canvas.height - y);

            if (state.config.zoomWithClickEnabled) {
                zoomWith(complexPoint, state.fractalConfig.defaultZoomScale, state);
            }
        }
        state.updateLater()
    }


    function mouseMoveCallback(e) {
        let m = getMouse(canvas, e);
        m.y = canvas.height - m.y;
        state.updateCoordinates(m);
    }


    function wheelMoveCallback(e) {
        eventMouseDown3D(e, state);
        let defaultScale = state.fractalConfig.defaultZoomScale;
        let scale = Math.sign(e.deltaY) === -1 ? defaultScale : 1 / defaultScale;
        if (((state.config.zoomOutComplexXMax > state.fractalConfig.uMax)
            && (state.config.zoomOutComplexYMax > state.fractalConfig.vMax))
            || (scale >= 1)
        ) {
            let x = state.animation.lastMouseX;
            let y = state.animation.lastMouseY;
            let complexPoint = getComplexPointFromRaster(state.fractalConfig, x, canvas.height - y);
            if (state.fractalConfig.zoomLocked) {
                // Complex point is 0.0
                complexPoint = {
                    x: 0.0,
                    y: 0.0
                };
            }
            zoomWith(complexPoint, scale, state);

            // Update sliders actual values
            if (sliders) {
                sliders.cmplxPlaneMaxX.updateValue(state.fractalConfig.uMax);
                sliders.cmplxPlaneMinX.updateValue(state.fractalConfig.uMin);
                sliders.cmplxPlaneMaxY.updateValue(state.fractalConfig.vMax);
                sliders.cmplxPlaneMinY.updateValue(state.fractalConfig.vMin);
            }

            // All needs update
            state.config.updateAll = true;
            // todo: see if this could be calculated, or is it better to hide!
            // state.config.divergenceLine.hidden = true;
            state.updateLater();
        } else {
            // add zoom info for zoom-in on unknown area
            // some kind of zoom bar?
            alert("Max zoom-out limit reached.");
        }

        e.preventDefault();
    }

    // Add listeners
    canDivLine.addEventListener("mousedown", mouseDownCallback, false);
    canDivLine.addEventListener("mousemove", mouseMoveCallback, false);
    window.addEventListener("mousedown", windowMouseDown, true);
    addWheelListener(canDivLine, wheelMoveCallback);
}

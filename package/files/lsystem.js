function LSystemRenderer(canvas) {
    let frontendUtil = new FrontendUtil();

    let jsonDataTree = {
        alphabet: ["R", "X", "F", "+", "-", "[", "]"],
        axiom: "CX",
        productions: {
            "X": "F+[[X]-X]-F[-FX]+XD",
            "F": "FF"
        },
        actions: {
            "F": new DrawCommand(1),
            "+": new RotateCommand(25),
            "-": new RotateCommand(-25),
            "[": new PushCommand(),
            "]": new PopCommand(),
            "R": new RandomColorCommand(),
            "D": new PointSizeCommand(0.005),
            "C": new ColorCommand("#5dfff5")
        },
        angle: 55,
        level: 8,
        unitLength: 0.6,
        unitLengthDegreeScaler: 1.0 / 2.1,
        origin: new Vector2D(0.0, 0.0),
        lineThickness: 0.0034
    };

    let jsonDataPlant = {
        alphabet: ["F", "+", "-", "[", "]"],
        axiom: "CF",
        productions: {
            "F": "FF+[+F-F-F]-[-F+F+F]D",
        },
        actions: {
            "F": new DrawCommand(1),
            "+": new RotateCommand(25),
            "-": new RotateCommand(-25),
            "[": new PushCommand(),
            "]": new PopCommand(),
            "D": new PointSizeCommand(0.07),
            "C": new ColorCommand("#ff93d6")
        },
        angle: 90,
        level: 5,
        unitLength: 0.3,
        unitLengthDegreeScaler: 1.0 / 2.05,
        origin: new Vector2D(0.5, 0.0),
        lineThickness: 0.001
    };

    let jsonDataTriangle = {
        alphabet: ["F", "G", "-", "+"],
        axiom: "CF-G-G",
        productions: {
            "F": "F-G+F+G-F",
            "G": "GG"
        },
        actions: {
            "F": new DrawCommand(1),
            "G": new DrawCommand(1),
            "+": new RotateCommand(120),
            "-": new RotateCommand(-120),
            "C": new ColorCommand("#ffe065")
        },
        angle: 0,
        level: 10,
        unitLength: 1.0,
        unitLengthDegreeScaler: 1.0 / 2.1,
        origin: new Vector2D(0.2, 0.8)
    };

    let jsonDataKoch = {
        alphabet: ["F", "+", "-"],
        axiom: "CF",
        productions: {
            "F": "F+F--F+F"
        },
        actions: {
            "F": new DrawCommand(1),
            "+": new RotateCommand(60),
            "-": new RotateCommand(-60),
            "C": new ColorCommand("#fff9fb")
        },
        angle: 0,
        level: 8,
        unitLength: 0.9,
        unitLengthDegreeScaler: 1.0 / 3.0,
        origin: new Vector2D(0.05, 0.4)
    };


    let jsonDataKochSnowflake = {
        alphabet: ["F", "+", "-"],
        axiom: "CF--F--F",
        productions: {
            "F": "F+F--F+F"
        },
        actions: {
            "F": new DrawCommand(1),
            "+": new RotateCommand(60),
            "-": new RotateCommand(-60),
            "C": new ColorCommand("#53fffc")
        },
        angle: 0,
        level: 7,
        unitLength: 0.9,
        unitLengthDegreeScaler: 1.0 / 3.05,
        origin: new Vector2D(0.10, 0.7)
    };


    let predefinedLSystems = [jsonDataTriangle, jsonDataPlant, jsonDataTree, jsonDataKoch, jsonDataKochSnowflake];

    let canvasWidth = 800;
    let canvasHeight = 600;

    this.fractalConfig = {
        // Raster coordinate system
        xMin: 0.0,
        xMax: canvasWidth,
        yMin: 0.0,
        yMax: canvasHeight,
        depthLimit: 1,

        lSystemType: L_SYSTEM_TYPE.PLANT_1,
    };

    // Init webGL
    let gl = initWebGL2(canvas);
    initWebGLSettings(gl);

    let program = initShaders(gl, "l-system-vertex-shader", "l-system-fragment-shader");

    // Setup locations
    let a_position_location = getAttribLocation(gl, program, "position");
    let a_color_location = getAttribLocation(gl, program, "color");
    let lSystemObj = {
        currentContext: undefined,
        positionInfo: {
            location: a_position_location,
            numOfCoordinates: 2,
            normalize: false,
            bufferData: undefined,
        },
        colorInfo: {
            location: a_color_location,
            numOfCoordinates: 4,
            normalize: false,
            bufferData: undefined,
        },
        vao: undefined
    };


    function updateCanvas() {
        myState.canvas.width = myState.fractalConfig.xMax;
        myState.canvas.height = myState.fractalConfig.yMax;
    }


    this.signalRendering = function (rendering) {
        myState.canvas.className = (rendering) ? "canvas-active" : "canvas-passive";
    };

    this.lastType = undefined;
    this.lastLimit = undefined;
    this.updateLater = function (afterFinish) {
        myState.signalRendering(true);

        if (myState.fractalConfig.lSystemType !== this.lastType || this.lastLimit !== myState.fractalConfig.depthLimit) {
            updateLData(myState.fractalConfig.lSystemType);
            initWebGLVAOBuffer(gl, lSystemObj);
            this.lastType = myState.fractalConfig.lSystemType;
            this.lastLimit = this.fractalConfig.depthLimit;
        }

        window.setTimeout(function () {
            myState.invalidate();
            if (afterFinish) {
                afterFinish();
            }
            myState.signalRendering(false);
        }, 100);
    };

    /**
     * Draws the scene.
     */
    this.invalidate = function () {
        updateCanvas();
        drawScene(gl);
    };


    function setUniforms(config, program) {
        gl.uniform1f(gl.getUniformLocation(program, "x_min"), config.xMin);
        gl.uniform1f(gl.getUniformLocation(program, "x_max"), config.xMax);
        gl.uniform1f(gl.getUniformLocation(program, "y_min"), config.yMin);
        gl.uniform1f(gl.getUniformLocation(program, "y_max"), config.xMax);
        gl.uniform2f(gl.getUniformLocation(program, "u_resolution"), config.xMax, config.yMax);
    }


    /**
     * The render function which draws the scene.
     * @param gl the WebG: context to draw the scene to
     */
    function drawScene(gl) {
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

        // Clear canvas
        gl.clear(gl.COLOR_BUFFER_BIT);

        // Use program
        gl.useProgram(program);

        // Set uniforms
        setUniforms(myState.fractalConfig, program);

        gl.bindVertexArray(lSystemObj.vao);
        let numOfPoints = lSystemObj.positionInfo.bufferData.length / lSystemObj.positionInfo.numOfCoordinates;
        if (lSystemObj.currentContext.enableLineTriangulization) {
            gl.drawArrays(gl.TRIANGLES, 0, numOfPoints);
        } else {
            gl.drawArrays(gl.LINES, 0, numOfPoints);
        }
    }


    /**
     * Initializes common webGL settings like clear color.
     * @param gl the webGL context to initialize settings for
     */
    function initWebGLSettings(gl) {
        let c = frontendUtil.getGLColorFromHex("#254b3c", 1.0);
        gl.clearColor(c[0], c[1], c[2], c[3]);
    }


    function bindAndAttachBufferData(gl, obj) {
        let buffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(obj.bufferData), gl.STATIC_DRAW);
        enableAttribForObj(gl, obj);

        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    }


    function initWebGLVAOBuffer(gl, obj) {
        obj.vao = gl.createVertexArray();
        gl.bindVertexArray(obj.vao);

        bindAndAttachBufferData(gl, obj.positionInfo);
        bindAndAttachBufferData(gl, obj.colorInfo);

        // Unbind the vao object
        gl.bindVertexArray(null);
    }


    function enableAttribForObj(gl, obj) {
        gl.enableVertexAttribArray(obj.location);
        gl.vertexAttribPointer(obj.location, obj.numOfCoordinates, gl.FLOAT, obj.normalize || false, 0, 0);
    }


    function updateLData(fractalType) {
        let jsonData = predefinedLSystems[fractalType];

        // Init turtle state
        let initialTurtleState = new TurtleState();
        setInitialValues(initialTurtleState, jsonData);

        const context = new TurtleContext();
        context.enableLineTriangulization = true;
        context.push(initialTurtleState);

        const system = new LSystem(jsonData);
        const finalGrammarState = system.generateState(jsonData.level);
        // const finalGrammarState = system.generateState(myState.fractalConfig.depthLimit);

        let actions = jsonData.actions;
        for (let c = 0; c < finalGrammarState.length; c++) {
            const ch = finalGrammarState[c];
            if (actions[ch]) {
                actions[ch].execute(context);
            }
        }

        lSystemObj.positionInfo.bufferData = context.vertexData;
        lSystemObj.colorInfo.bufferData = context.colorData;
        lSystemObj.currentContext = context;
    }


    let myState = this;
    myState.canvas = canvas;

    updateCanvas();


    return myState;
}

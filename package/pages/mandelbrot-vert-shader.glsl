#version 300 es

// =============================================
// === Mandelbrot Fractal Vertex Shader
// =============================================

// Input attributes
in vec2 a_position;

void main(){
    gl_Position = vec4(a_position, 0.0, 1.0);
    gl_PointSize = 1.0;
}
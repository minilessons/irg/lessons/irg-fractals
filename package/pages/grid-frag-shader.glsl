#version 300 es

// =============================================
// === Complex Grid Fragment Shader
// =============================================
precision mediump float;

uniform vec4 color;

out vec4 out_color;

void main(){
    // Set output color
    out_color = color;
}


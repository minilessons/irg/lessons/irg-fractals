#version 300 es

// =============================================
// === Julia Fractal Fragment Shader
// =============================================

precision mediump float;
const int MAX_DEPTH_LIMIT = 20000;

// Raster coordinate system
uniform float xMin;
uniform float xMax;
uniform float yMin;
uniform float yMax;

// Complex coordinate system
uniform float uMin;
uniform float uMax;
uniform float vMin;
uniform float vMax;

// Depth limit for divergence test
uniform int limit;

uniform vec2 initial_complex_number;
uniform int color_sheme;

// Powers for mandelbrot
uniform float exponent;
const float gamma = 3.5;

// Function declarations
void z_power(inout vec2 z, float n);
int divergence_test(vec2 z0, vec2 c, out vec2 zn, float epsilonSquare);
void divergence_test_histogram(vec2 z0, vec2 c, float epsilon, out float smoothColor);
vec3 color_sheme_basic(int n);
vec3 color_sheme_red(int n);
vec3 color_sheme_histogram(int n, vec2 z0, vec2 zn, float escapeRadius);
vec3 getNormalizedRGB(int r, int g, int b);
vec3 simple_color_sheme(int n);
vec3 paletteWithMath(float t, vec3 a, vec3 b, vec3 c, vec3 d);
vec3 palette(float t, vec3 c1, vec3 c2, vec3 c3, vec3 c4);
vec3 gray_scheme(int n);
vec3 black_white_sheme(int n);
vec3 hsvToRgb(vec3 hsv);
float complexModul(vec2 z);


out vec4 out_color;
void main(){
        vec2 a_position = gl_FragCoord.xy;
        float epsilon = max(initial_complex_number.x * initial_complex_number.x + initial_complex_number.y * initial_complex_number.y, 2.0);
        float epsilonSquare = epsilon * epsilon;

        vec2 z0 = vec2(0.0, 0.0);
        vec2 zn = vec2(0.0, 0.0);
        z0.x = (a_position.x - xMin) / (xMax - xMin) * (uMax - uMin) + uMin;
        z0.y = (a_position.y - yMin) / (yMax - yMin) * (vMax - vMin) + vMin;
        vec3 color;
        int n = 0;
        if(color_sheme == 4){
        float smoothColor = 0.0;
        divergence_test_histogram(z0, initial_complex_number, epsilon, smoothColor);
        smoothColor /= float(limit);
        color = hsvToRgb(vec3(0.95 + 120.0 * smoothColor, 0.8, 1.0));
        } else {
            n = divergence_test(z0, initial_complex_number, zn, epsilonSquare);
            if (color_sheme == 0) {
                color = color_sheme_red(n);
            } else if (color_sheme == 1) {
                color = simple_color_sheme(n);
            } else if (color_sheme == 2) {
                color = color_sheme_basic(n);
            } else if (color_sheme == 3) {
                color = black_white_sheme(n);
            } else if(color_sheme == 5){
                color = palette(float(n) / float(limit), vec3(0.02, 0.02, 0.03), vec3(0.1, 0.2, 0.3), vec3(0.0, 0.3, 0.2), vec3(0.0, 0.5, 0.8));
                color = vec3(pow(color.x, 1.0 / gamma), pow(color.y, 1.0 / gamma), pow(color.z, 1.0 / gamma));
            } else if (color_sheme == 6) {
                // Grayed out
                color = gray_scheme(n);
            } else {
                color = black_white_sheme(n);
            }
        }

        out_color = vec4(color, 1.0);
}

float complexModul(vec2 z) {
    return sqrt(z.x * z.x + z.y * z.y);
}


vec3 paletteWithMath(float t, vec3 a, vec3 b, vec3 c, vec3 d) {
        return a + b * cos(6.28318 * (c * t + d));
}

// Linear interpolation between four colours
vec3 palette(float t, vec3 c1, vec3 c2, vec3 c3, vec3 c4) {
    float x = 1.0 / 3.0;
    if (t < x) {
        return mix(c1, c2, t / x);
    } else if (t < 2.0 * x) {
        return mix(c2, c3, (t - x) / x);
    } else if (t < 3.0 * x) {
            return mix(c3, c4, (t - 2.0 * x) / x);
    }

    return c4;
}

vec3 getNormalizedRGB(int r, int g, int b) {
    return vec3(float(r) / 255.0, float(g) / 255.0, float(b) / 255.0);
}

void divergence_test_histogram(vec2 z0, vec2 c, float epsilon, out float smoothColor){
    vec2 z = vec2(z0.x, z0.y);
    float modul2 = z.x * z.x + z.y * z.y;
    if (modul2 > epsilon * epsilon) {
        return;
    }

    smoothColor = exp(-complexModul(z));
    float modul;
    for (int i = 0; i <= MAX_DEPTH_LIMIT; i++) {
        if(i >= limit){
            break;
        }
        z_power(z, exponent);
        z += c;

        modul = complexModul(z);
        smoothColor += exp(-modul);
        if (modul > epsilon) {
            return;
        }
    }

    return;
}

int divergence_test(vec2 z0, vec2 c, out vec2 zn, float epsilonSquare) {
    vec2 z = vec2(z0.x, z0.y);
    float modul2 = z.x * z.x + z.y * z.y;
    if (modul2 > epsilonSquare) {
        return 0;
    }

    float modulSquared;
    for (int i = 0; i <= MAX_DEPTH_LIMIT; i++) {
        if(i >= limit){
            break;
        }
        z_power(z, exponent);
        z += c;

        modulSquared = z.x * z.x + z.y * z.y;
        if (modulSquared > epsilonSquare) {
            zn = z;
            return i;
        }
    }

    zn = z;
    return -1;
}

vec3 gray_scheme(int n) {
    if (n == -1) {
        return vec3(0.662, 0.662, 0.662);
    } else {
        return vec3(1.0, 1.0, 1.0);
    }
}


vec3 black_white_sheme(int n) {
    if (n == -1) {
        return vec3(0.0, 0.0, 0.0);
    } else {
        return vec3(1.0, 1.0, 1.0);
    }
}


vec3 hsvToRgb(vec3 c) {
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 color_sheme_histogram(int n, vec2 z0, vec2 zn, float escapeRadius){
    float abszn = sqrt((zn.x * zn.x + zn.y * zn.y));
    float sm = float(n + 1) - log(log(abszn)) / log(escapeRadius);
    sm = sm / float(limit);
    return hsvToRgb(vec3(0.95 + 10.0 * sm, 0.8, 1.0));
}

vec3 simple_color_sheme(int n) {
    if (n == -1) {
        return vec3(0.0, 0.0, 0.0);
    } else {
        float n_divide_limit = float(n) / float(limit);
        float r = n_divide_limit;
        float g = 1.0 - n_divide_limit / 2.0;
        float b = 0.8 - n_divide_limit / 3.0;
        return vec3(r, g, b);
    }
}

vec3 color_sheme_red(int n) {
    if (n == -1) {
        return vec3(0.0, 0.0, 0.0);
    } else if (limit < 16) {
        int r = int(float((n - 1)) / float(limit - 1) * 255.0 + 0.5);
        int g = 255 - r;
        float b = mod(float(n - 1), float(limit / 2) * 255.0 / float(limit / 2));

        return vec3(float(r) / 255.0, float(g) / 255.0, float(b) / 255.0);
    } else {
        int lim = (limit < 32) ? limit : 32;
        float r = float(n - 1) * 255.0 / float(lim);
        float g = mod(float(n - 1), float(lim / 4) * 255.0 / float(lim / 4));
        float b = mod(float(n - 1), float(lim / 8) * 255.0 / float(lim / 8));
        return vec3(r / 255.0, g / 255.0, b / 255.0);
    }
}

vec3 color_sheme_basic(int n) {
    if (n == -1) {
        return vec3(0.0, 0.0, 0.0);
    } else if (limit < 16) {
        int r = int(float((n - 1)) / float(limit - 1) * 255.0 + 0.5);
        int g = 255 - r;
        int b = int(mod(float(n - 1), float(limit / 2))) * 255 / (limit / 2);
        return getNormalizedRGB(r, g, b);
    } else {
        int lim = (limit < 32) ? limit : 32;
        int r = (n - 1) * 255 / lim;
        int g = int(mod(float(n - 1), float(lim / 4))) * 255 / (lim / 4);
        int b = int(mod(float(n - 1), float(lim / 8))) * 255 / (lim / 8);
        return getNormalizedRGB(r, g, b);
    }
}

void z_power(inout vec2 z, float n){
    float modul = sqrt(z.x * z.x + z.y * z.y);
    float r = pow(modul, n);
    float newAngle = n * atan(z.y, z.x);
    z.x = r * cos(newAngle);
    z.y = r * sin(newAngle);
}
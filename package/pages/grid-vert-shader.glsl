#version 300 es

// =============================================
// === Complex Grid Vertex Shader
// =============================================

// Input attributes
in vec2 a_position;

uniform float point_size;
uniform vec2 u_resolution;

void main(){
    vec2 zeroToOne = a_position / u_resolution;
    vec2 zeroToTwo = zeroToOne * 2.0;
    vec2 clipSpace = zeroToTwo - 1.0;
 
    gl_Position = vec4(clipSpace, 0.0, 1.0);
    gl_PointSize = point_size;
}
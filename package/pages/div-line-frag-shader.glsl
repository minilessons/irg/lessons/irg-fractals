#version 300 es

// =============================================
// === Divergence line Fragment Shader
// =============================================
precision mediump float;


uniform vec4 color;
uniform bool point_shading;

out vec4 out_color;

void main(){
    float r = 0.0;
    float delta = 0.0;
    float alpha = 1.0;
    vec2 cxy = 2.0 * gl_PointCoord - 1.0;
    r = dot(cxy, cxy);
    if(r > 1.0 && point_shading){
        discard;
    }

    #ifdef GL_OES_standard_derivatives
        delta = fwidth(r);
        alpha = 1.0 - smoothstep(1.0 - delta, 1.0 + delta, r);
    #endif

    // Set output color
    if(point_shading == true) {
        out_color = color *  (alpha);
    } else {
        out_color = color;
    }
}

